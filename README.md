# Mr Thrower v0.2
Windows client of an application for wireless communication between computers and mobile devices.

## About the app
Author         | Alex Kazakov / <kazakov24alex@gmail.com>
-------------- | --------------
OS             | Windows
Language	   | Java
RDBMS          | SQLite
Localization   | EN, RU
GUI Framework  | JavaFX        
Libraries	   | Event Bus, BlueCove, SQLite JDBC
Android client | <https://bitbucket.org/kazakov24alex/mr-thrower-android-client/src>
Own library	   | <https://bitbucket.org/kazakov24alex/mr-thrower-communication-library/src>
Draft GUI      | <https://app.moqups.com/kazakov24alex@gmail.com/tov61iLnYl/view/page/ad64222d5>

## Functionality
+ identification of devices using this application
+ transfer files and text messages between computers and mobile devices using:
	+ Wi-Fi network
	+ Bluetooth
+ history of all sent messages and files
+ open received files directly from the application
+ copying the received message to the clipboard

## TODO
+ network change handler
+ check permissions runtime
+ deleting received messages and files
+ settings
	+ nickname change
	+ download folder change
	+ download folder chooser
+ code
	+ history mode refactor (clear old version)
	
## Way of development
+ v0.3 - add Wi-Fi Direct mode
+ v1.0 - make portable EXE file
