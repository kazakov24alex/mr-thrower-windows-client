package com.mr_thrower.application.fragments.chat_side;

import com.mr_thrower.application.database.DbManager;
import com.mr_thrower.application.fragments.contacts_side.ContactFragmentController;
import library.codes.PacketCodes;
import library.objects.BaseProtocolObject;
import library.wrappers.MessageWrapper;
import library.wrappers.ThrowWrapper;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import library.codes.ThrowStateCodes;
import library.objects.ThrowEndAckObject;
import library.objects.ThrowProgressObject;
import library.wrappers.MacAddress;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Vector;

public class ChatFragmentController extends AnchorPane {
    private static Logger log = Logger.getLogger(ChatFragmentController.class.getName());
    private ClassLoader loader;


//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private static final String FXML_PATH_CHAT_FRAGMENT = "fragments/chat_fragment.fxml";

    private static final String STYLESHEET_PATH = "styles/chat_fragment.css";
    private static final String STYLE_LABEL = "label";
    private static final String STYLE_CHAT_CONTAINER = "chat-container";


//======================================================================================================================
// FXML NODES
//======================================================================================================================

    @FXML private AnchorPane root;
    @FXML private ScrollPane scrollPane;
    @FXML private VBox container;


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private MacAddress macAddress;
    private int counter;

    private Vector<MessageFragmentController> messageFCVector;
    private Vector<ThrowFragmentController> throwFCVector;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public ChatFragmentController() {
        loader = getClass().getClassLoader();

        FXMLLoader fxmlLoader = new FXMLLoader(loader.getResource(FXML_PATH_CHAT_FRAGMENT));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        setStyles();

        messageFCVector = new Vector<>();
        throwFCVector = new Vector<>();

        counter = 0;

        scrollPane.vvalueProperty().bind(container.heightProperty());
    }


//======================================================================================================================
// SETTERS
//======================================================================================================================

    public void setStyles() {
        getStylesheets().add(loader.getResource(STYLESHEET_PATH).toExternalForm());

        scrollPane.getStyleClass().add(STYLE_CHAT_CONTAINER);
    }

    public void changeContact(MacAddress macAddress) {
        this.macAddress = macAddress;

        clearChat();
        uploadFromDatabase(macAddress);
    }

    public void pushMessage(MessageWrapper message) {
        MessageFragmentController messageFC = new MessageFragmentController(message);

        messageFCVector.add(messageFC);
        container.getChildren().add(counter++, messageFC);
        //scrollPane.setVvalue(1.0);
    }

    public void pushThrow(ThrowWrapper throwWr) {
        ThrowFragmentController throwFC = new ThrowFragmentController(throwWr);
        throwFCVector.add(throwFC);
        container.getChildren().add(counter++, throwFC);
        //scrollPane.setVvalue(1.0);
    }

    public void pushProgress(ThrowProgressObject progress) {
        for(ThrowFragmentController throwFC : throwFCVector) {
            if(throwFC.getTimeID().getTime() == progress.getTimeID().getTime()) {
                throwFC.setProgress(progress.getValue());
                return;
            }
        }
    }

    public void pushEndAck(ThrowEndAckObject endAck) {
        for(ThrowFragmentController throwFC : throwFCVector) {
            if(throwFC.getTimeID().getTime() == endAck.getTimeID().getTime()) {
                throwFC.setState(ThrowStateCodes.STATE_FINISHED);
                return;
            }
        }
    }


    public void uploadFromDatabase(MacAddress macAddress) {
        ArrayList<BaseProtocolObject> objects = new ArrayList<>();
        objects.addAll(DbManager.getInstance().selectMessagesByMac(macAddress));
        objects.addAll(DbManager.getInstance().selectThrowsByMac(macAddress));

        Collections.sort(objects, baseProtocolObjectComparator);

        for(BaseProtocolObject object : objects) {
            if(object.getCode() == PacketCodes.MESSAGE)
                pushMessage((MessageWrapper) object);
            else
                pushThrow((ThrowWrapper) object);
        }
    }

    public void clearChat() {
        for(MessageFragmentController mesFC : messageFCVector) {
            container.getChildren().remove(mesFC);
        }
        messageFCVector.clear();

        for(ThrowFragmentController throwFC : throwFCVector) {
            container.getChildren().remove(throwFC);
        }
        throwFCVector.clear();

        counter = 0;
    }


//======================================================================================================================
// COMPARATOR
//======================================================================================================================

    public static Comparator<BaseProtocolObject> baseProtocolObjectComparator = new Comparator<BaseProtocolObject>() {

        public int compare(BaseProtocolObject a, BaseProtocolObject b) {
            long timeID1 = 0;
            long timeID2 = 0;

            if(a.getCode() == PacketCodes.MESSAGE)
                timeID1 = ((MessageWrapper) a).getTimeID().getTime();
            else
                timeID1 = ((ThrowWrapper) a).getTimeID().getTime();

            if(b.getCode() == PacketCodes.MESSAGE)
                timeID2 = ((MessageWrapper) b).getTimeID().getTime();
            else
                timeID2 = ((ThrowWrapper) b).getTimeID().getTime();

            if(timeID1 > timeID2)
                return 1;
            else
                return -1;
        }};
}
