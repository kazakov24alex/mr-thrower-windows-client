package com.mr_thrower.application.fragments.chat_side;

import library.wrappers.MessageWrapper;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import org.apache.log4j.Logger;

import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import library.codes.DirectionCodes;
import library.codes.ThrowModeCodes;


public class MessageFragmentController extends AnchorPane {
    private static Logger log = Logger.getLogger(MessageFragmentController.class.getName());
    private ClassLoader loader;


//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private static final String PATH_FXML_MESSAGE_FRAGMENT = "fragments/message_fragment.fxml";

    private static final String PATH_STYLESHEET = "styles/message_fragment.css";

    private static final String STYLE_BOX_WIFI_IN = "box-wifi-in";
    private static final String STYLE_BOX_WIFI_OUT = "box-wifi-out";
    private static final String STYLE_BOX_BT_IN = "box-bt-in";
    private static final String STYLE_BOX_BT_OUT = "box-bt-out";
    private static final String STYLE_BOX_DIRECT_IN = "box-direct-in";
    private static final String STYLE_BOX_DIRECT_OUT = "box-direct-out";

    private static final String STYLE_MESSAGE_WIFI = "message-text-wifi";
    private static final String STYLE_MESSAGE_BT = "message-text-bt";
    private static final String STYLE_MESSAGE_DIRECT = "message-text-direct";

    private static final String STYLE_TIME_WIFI = "time-wifi";
    private static final String STYLE_TIME_BT = "time-bt";
    private static final String STYLE_TIME_DIRECT = "time-direct";


//======================================================================================================================
// FXML NODES
//======================================================================================================================

    @FXML private AnchorPane root;
    @FXML private AnchorPane box;
    @FXML private Label message;
    @FXML private Label time;


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private MessageWrapper messageWr;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public MessageFragmentController(MessageWrapper messageWr) {
        loader = getClass().getClassLoader();
        this.messageWr = messageWr;

        FXMLLoader fxmlLoader = new FXMLLoader(loader.getResource(PATH_FXML_MESSAGE_FRAGMENT));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        setStyles();

        this.messageWr = messageWr;
        setTime(messageWr.getTimeID());
        setMessage(messageWr.getText());
        setDirection(messageWr.getDir());
    }


//======================================================================================================================
// SETTERS
//======================================================================================================================

    public void setStyles() {
        getStylesheets().add(loader.getResource(PATH_STYLESHEET).toExternalForm());

        String boxStyle = "";
        String textStyle = "";
        String timeStyle = "";

        switch (messageWr.getMode()) {
            case ThrowModeCodes.WIFI_NETWORK:
                textStyle = STYLE_MESSAGE_WIFI;
                timeStyle = STYLE_TIME_WIFI;
                if(messageWr.getDir() == DirectionCodes.INCOMING)
                    boxStyle = STYLE_BOX_WIFI_IN;
                else
                    boxStyle = STYLE_BOX_WIFI_OUT;
                break;
            case ThrowModeCodes.BLUETOOTH:
                textStyle = STYLE_MESSAGE_BT;
                timeStyle = STYLE_TIME_BT;
                if(messageWr.getDir() == DirectionCodes.INCOMING)
                    boxStyle = STYLE_BOX_BT_IN;
                else
                    boxStyle = STYLE_BOX_BT_OUT;
                break;
            case ThrowModeCodes.WIFI_DIRECT:
                timeStyle = STYLE_TIME_DIRECT;
                textStyle = STYLE_MESSAGE_DIRECT;
                if(messageWr.getDir() == DirectionCodes.INCOMING)
                    boxStyle = STYLE_BOX_DIRECT_IN;
                else
                    boxStyle = STYLE_BOX_DIRECT_OUT;
                break;

        }

        box.getStyleClass().add(boxStyle);
        message.getStyleClass().add(textStyle);
        time.getStyleClass().add(timeStyle);
    }

    public void setMessage(String str) {
        message.setText(str);
    }

    public void setTime(Date timeID) {
        Date currentDate = new Date();
        String oldstring = "";

        Calendar calTimeID = Calendar.getInstance();
        calTimeID.setTime(timeID);
        Calendar calCurrent = Calendar.getInstance();
        calCurrent.setTime(currentDate);

        if(calTimeID.get(Calendar.YEAR) == calCurrent.get(Calendar.YEAR)
                && calTimeID.get(Calendar.MONTH) == calCurrent.get(Calendar.MONTH)
                && calTimeID.get(Calendar.DATE) == calCurrent.get(Calendar.DATE)) {
            SimpleDateFormat df = new SimpleDateFormat("HH:mm");
            time.setText(df.format(timeID));

        } else if(calTimeID.get(Calendar.YEAR) == calCurrent.get(Calendar.YEAR)) {
            SimpleDateFormat df = new SimpleDateFormat("d MMM HH:mm");
            time.setText(df.format(timeID));

        } else {
            SimpleDateFormat df = new SimpleDateFormat("d MMM yyyy HH:mm");
            time.setText(df.format(timeID));
        }

    }

    public void setDirection(Byte direction) {
        switch(direction) {
            case DirectionCodes.INCOMING:
                root.setLeftAnchor(box, 0.0);
                break;
            case DirectionCodes.OUTCOMING:
                root.setRightAnchor(box, 0.0);
                break;
        }

    }



    @FXML
    public void onMessageClicked() {
        String stringToClipboard = message.getText();

        StringSelection stringSelection = new StringSelection(stringToClipboard);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(stringSelection, null);
    }

}
