package com.mr_thrower.application.fragments.chat_side;

import com.mr_thrower.application.scenes.MainSceneController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import org.apache.log4j.Logger;

import java.io.IOException;

public class TitleBarFragmentController extends AnchorPane {
    private static Logger log = Logger.getLogger(InputLineFragmentController.class.getName());
    private ClassLoader loader;


//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private static final String FXML_PATH = "fragments/title_bar_fragment.fxml";

    private static final String STYLESHEET_PATH = "styles/title_bar_fragment.css";
    private static final String STYLE_TILE = "tile-yellow";
    private static final String STYLE_LABEL = "label";

    private static final String IMAGE_PATH_FILTER = "images/filter_32.png";
    private static final String IMAGE_PATH_CLOSE = "images/close_32.png";


//======================================================================================================================
// FXML NODES
//======================================================================================================================

    @FXML private AnchorPane container;
    @FXML private Label contactNameLabel;
    @FXML private ImageView filterImageView;
    @FXML private ImageView closeImageView;


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private MainSceneController wifiNSC;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public TitleBarFragmentController(MainSceneController wifiNSC) {
        loader = getClass().getClassLoader();
        this.wifiNSC = wifiNSC;

        FXMLLoader fxmlLoader = new FXMLLoader(loader.getResource(FXML_PATH));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        setImages();
        setStyles();
    }


//======================================================================================================================
// SETTERS
//======================================================================================================================

    public void setTitle(String title) {
        contactNameLabel.setText(title);
    }

    public void setImages() {
        Image filterImage = new Image(loader.getResource(IMAGE_PATH_FILTER).toString());
        filterImageView.setImage(filterImage);

        Image closeImage = new Image(loader.getResource(IMAGE_PATH_CLOSE).toString());
        closeImageView.setImage(closeImage);
    }

    public void setStyles() {
        getStylesheets().add(loader.getResource(STYLESHEET_PATH).toExternalForm());

        container.getStyleClass().add(STYLE_TILE);
        contactNameLabel.getStylesheets().add(STYLE_LABEL);
    }


//======================================================================================================================
// CALLBACKS
//======================================================================================================================

    @FXML
    private void onCrossClicked() {
        wifiNSC.onChatClosed();
    }

    @FXML
    private void onFilterClicked() {

    }

}
