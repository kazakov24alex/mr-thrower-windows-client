package com.mr_thrower.application.fragments.chat_side;

import com.mr_thrower.application.database.DbManager;
import javafx.scene.image.Image;
import library.codes.ThrowModeCodes;
import library.objects.ThrowFileObject;
import library.wrappers.ThrowWrapper;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import library.codes.DirectionCodes;
import library.codes.ThrowStateCodes;
import org.apache.log4j.Logger;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Date;

public class ThrowFragmentController extends AnchorPane {
    private static Logger log = Logger.getLogger(ThrowFragmentController.class.getName());
    private ClassLoader loader;


//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private static final String PATH_FXML_FILE_THROW_FRAGMENT = "fragments/file_throw_fragment.fxml";

    private static final String PATH_STYLESHEET = "styles/file_throw_fragment.css";

    private static final String PATH_IMAGE_FILE_BLACK = "images/file_black_64.png";
    private static final String PATH_IMAGE_FILE_WHITE = "images/file_white_64.png";

    private static final String STYLE_BOX_WIFI_IN = "box-wifi-in";
    private static final String STYLE_BOX_WIFI_OUT = "box-wifi-out";
    private static final String STYLE_BOX_BT_IN = "box-bt-in";
    private static final String STYLE_BOX_BT_OUT = "box-bt-out";
    private static final String STYLE_BOX_DIRECT_IN = "box-direct-in";
    private static final String STYLE_BOX_DIRECT_OUT = "box-direct-out";

    private static final String STYLE_PROGRESS_BAR_WIFI = "progressbar_wifi";
    private static final String STYLE_PROGRESS_BAR_BT = "progressbar_bt";
    private static final String STYLE_PROGRESS_BAR_DIRECT = "progressbar_direct";

    private static final String STYLE_MESSAGE_WIFI = "data-wifi";
    private static final String STYLE_MESSAGE_BT = "data-bt";
    private static final String STYLE_MESSAGE_DIRECT = "data-direct";
    private static final String STYLE_TIME_TEXT = "time";



//======================================================================================================================
// FXML NODES
//======================================================================================================================

    @FXML private AnchorPane container;
    @FXML private HBox box;
    @FXML private ImageView fileLogoImageView;
    @FXML private Label filenameLabel;
    @FXML private ProgressBar progressbarView;
    @FXML private Label stateLabel;
    @FXML private Label sizeLabel;


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private ThrowWrapper throwWr;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public ThrowFragmentController(ThrowWrapper throwWr) {
        loader = getClass().getClassLoader();
        this.throwWr = throwWr;

        FXMLLoader fxmlLoader = new FXMLLoader(loader.getResource(PATH_FXML_FILE_THROW_FRAGMENT));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        setStyles();
        setImage(throwWr.getMode());

        this.throwWr = throwWr;
        filenameLabel.setText(throwWr.getFilename());
        setDirection(throwWr.getDir());
        setState(throwWr.getState());
    }


//======================================================================================================================
// SETTERS
//======================================================================================================================

    public Date getTimeID() {
        return throwWr.getTimeID();
    }


//======================================================================================================================
// SETTERS
//======================================================================================================================

    public void setStyles() {
        String boxStyle = "";
        String textStyle = "";
        String progressBarStyle = "";

        switch (throwWr.getMode()) {
            case ThrowModeCodes.WIFI_NETWORK:
                textStyle = STYLE_MESSAGE_WIFI;
                progressBarStyle = STYLE_PROGRESS_BAR_WIFI;
                if(throwWr.getDir() == DirectionCodes.INCOMING)
                    boxStyle = STYLE_BOX_WIFI_IN;
                else
                    boxStyle = STYLE_BOX_WIFI_OUT;
                break;
            case ThrowModeCodes.BLUETOOTH:
                textStyle = STYLE_MESSAGE_BT;
                progressBarStyle = STYLE_PROGRESS_BAR_BT;
                if(throwWr.getDir() == DirectionCodes.INCOMING)
                    boxStyle = STYLE_BOX_BT_IN;
                else
                    boxStyle = STYLE_BOX_BT_OUT;
                break;
            case ThrowModeCodes.WIFI_DIRECT:
                textStyle = STYLE_MESSAGE_DIRECT;
                progressBarStyle = STYLE_PROGRESS_BAR_DIRECT;
                if(throwWr.getDir() == DirectionCodes.INCOMING)
                    boxStyle = STYLE_BOX_DIRECT_IN;
                else
                    boxStyle = STYLE_BOX_DIRECT_OUT;
                break;

        }

        getStylesheets().add(loader.getResource(PATH_STYLESHEET).toExternalForm());

        box.getStyleClass().add(boxStyle);
        filenameLabel.getStyleClass().add(textStyle);
        stateLabel.getStyleClass().add(textStyle);
        sizeLabel.getStyleClass().add(textStyle);
        progressbarView.getStyleClass().add(progressBarStyle);
    }

    public void setImage(Byte mode) {
        switch (mode) {
            case ThrowModeCodes.WIFI_NETWORK:
                fileLogoImageView.setImage(new Image(PATH_IMAGE_FILE_BLACK));
                break;
            case ThrowModeCodes.BLUETOOTH:
                fileLogoImageView.setImage(new Image(PATH_IMAGE_FILE_WHITE));
                break;
            case ThrowModeCodes.WIFI_DIRECT:
                fileLogoImageView.setImage(new Image(PATH_IMAGE_FILE_BLACK));
                break;
        }
    }



    public void setDirection(Byte direction) {
        switch(direction) {
            case DirectionCodes.INCOMING:
                container.setLeftAnchor(box, 0.0);
                break;
            case DirectionCodes.OUTCOMING:
                container.setRightAnchor(box, 0.0);
                break;
        }
    }

    public void setProgress(long progress) {
        throwWr.setState(ThrowStateCodes.STATE_PROCESS);
        throwWr.setCurrentSize(progress);
        progressbarView.setProgress(throwWr.getProgress()*0.01);

        sizeLabel.setText(ThrowFileObject.sizeStringFormat(progress, 2) + " / "+ThrowFileObject.sizeStringFormat(throwWr.getFileSize(), 2));
    }

    public void setState(Byte state) {
        switch (state) {
            case ThrowStateCodes.STATE_REQUEST:
                stateLabel.setText(ThrowStateCodes.STATE_REQUEST_TITLE);
                setProgress(0);
                break;
            case ThrowStateCodes.STATE_REJECTED:
                stateLabel.setText(ThrowStateCodes.STATE_REJECTED_TITLE);
                setProgress(0);
                break;
            case ThrowStateCodes.STATE_PROCESS:
                stateLabel.setText(ThrowStateCodes.STATE_PROCESS_TITLE);
                setProgress(0);
                break;
            case ThrowStateCodes.STATE_INTERRUPTED:
                stateLabel.setText(ThrowStateCodes.STATE_INTERRUPTED_TITLE);
                setProgress(0);
                break;
            case ThrowStateCodes.STATE_FINISHED:
                setProgress(throwWr.getFileSize());
                stateLabel.setText(ThrowStateCodes.STATE_FINISHED_TITLE);
                setProgress(throwWr.getFileSize());
                break;
            case ThrowStateCodes.STATE_DELETED:
                stateLabel.setText(ThrowStateCodes.STATE_DELETED_TITLE);
                setProgress(0);
                break;
        }
    }



    @FXML
    public void onThrowClicked() {
        ThrowWrapper tFile = DbManager.getInstance().selectThrowsByTimeID(throwWr.getAddress(), throwWr.getTimeID());

        if(tFile.getPath() != null) {
            try {
                log.info("Opening file ...");
                Desktop.getDesktop().open(new File(tFile.getPath()));
            } catch(IOException e) {
                log.info("Can not open explorer...");
            }
        }

    }
}
