package com.mr_thrower.application.fragments.chat_side;

import library.wrappers.MessageWrapper;
import library.wrappers.ThrowWrapper;
import library.codes.DirectionCodes;
import library.codes.ThrowModeCodes;
import library.codes.ThrowTypeCodes;
import library.objects.MessageObject;
import library.objects.ThrowFileObject;
import library.wrappers.EventWrapper;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.codec.digest.DigestUtils;
import com.mr_thrower.application.SceneSelector;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TextArea;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import library.objects.ThrowRequestObject;
import library.wrappers.MD5;
import library.wrappers.MacAddress;
import org.apache.log4j.Logger;
import org.greenrobot.eventbus.EventBus;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

public class InputLineFragmentController extends AnchorPane {
    private static Logger log = Logger.getLogger(InputLineFragmentController.class.getName());
    private ClassLoader loader;


//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private static final String FXML_PATH_INPUT_LINE_FRAGMENT = "fragments/input_line_fragment.fxml";

    private static final String STYLESHEET_PATH = "styles/input_line_fragment.css";
    private static final String STYLE_TILE_YELLOW = "tile-yellow";

    private static final String IMAGE_PATH_ATTACH = "images/attach_32.png";
    private static final String IMAGE_PATH_SEND = "images/send_32.png";


//======================================================================================================================
// FXML NODES
//======================================================================================================================

    @FXML private AnchorPane container;
    @FXML private TextArea messageTextArea;
    @FXML private ImageView attachImageView;
    @FXML private ImageView sendImageView;


//======================================================================================================================
// FXML NODES
//======================================================================================================================

    private Byte mode;

    private MacAddress macAddress;
    private ChatFragmentController chatFC;

//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public InputLineFragmentController(Byte mode) {
        loader = getClass().getClassLoader();
        this.mode = mode;

        FXMLLoader fxmlLoader = new FXMLLoader(loader.getResource(FXML_PATH_INPUT_LINE_FRAGMENT));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        if(mode == ThrowModeCodes.WIFI_DIRECT) {
            messageTextArea.setDisable(true);
            sendImageView.setDisable(true);
            attachImageView.setDisable(true);
        }

        setImages();
        setStyles();
    }


//======================================================================================================================
// SETTERS
//======================================================================================================================

    public void setImages() {
        Image attachImage = new Image(loader.getResource(IMAGE_PATH_ATTACH).toString());
        attachImageView.setImage(attachImage);

        Image sendImage = new Image(loader.getResource(IMAGE_PATH_SEND).toString());
        sendImageView.setImage(sendImage);

    }

    public void setStyles() {
        getStylesheets().add(loader.getResource(STYLESHEET_PATH).toExternalForm());

        container.getStyleClass().add(STYLE_TILE_YELLOW);
    }

    public void changeContact(MacAddress macAddress, ChatFragmentController chatFC) {
        this.macAddress = macAddress;
        this.chatFC = chatFC;
    }



//======================================================================================================================
// CALLBACKS
//======================================================================================================================

    @FXML
    private void onAttachClicked() {
        if(mode == ThrowModeCodes.WIFI_DIRECT)
            return;

        List<File> filesList = SceneSelector.getInstance().showFileChooser();
        if(filesList == null)
            return;


        long currentMs = new Date().getTime();
        ThrowRequestObject request = new ThrowRequestObject();


        for(int i = 0; i < filesList.size(); i++) {
            File file = filesList.get(i);

            try {
                FileInputStream fis = new FileInputStream(file);
                String md5 = DigestUtils.md5Hex(fis);
                fis.close();
                //byte[] md5bytes = Hex.decodeHex(md5.toCharArray());

                ThrowFileObject tFile = new ThrowFileObject(new Date(currentMs + i), file.getName(), file.length(), new MD5("595C4ABC18EDB68E9441547A653CDC4A"));
                tFile.setPath(file.getPath());
                tFile.setAddress(macAddress);

                request.addThrowFile(tFile);

                ThrowWrapper throwWr = new ThrowWrapper(tFile, DirectionCodes.OUTCOMING, mode, ThrowTypeCodes.FILE);
                chatFC.pushThrow(throwWr);

            } catch (Exception e) {
                log.error(e.getMessage());
                e.printStackTrace();
            }
        }

        request.setAddress(macAddress);
        EventBus.getDefault().post(new EventWrapper(request, EventWrapper.TO_EVENT_MANAGER));
    }

    @FXML
    private void onMessageSendClicked() {
        // ban on sending empty messages
        if(messageTextArea.getText().isEmpty())
            return;

        MessageObject message = new MessageObject(new Date(), messageTextArea.getText());
        message.setAddress(macAddress);
        MessageWrapper messageWr = new MessageWrapper(message, DirectionCodes.OUTCOMING, mode);

        chatFC.pushMessage(messageWr);
        EventBus.getDefault().post(new EventWrapper(message, EventWrapper.TO_EVENT_MANAGER));


        messageTextArea.clear();
    }



}
