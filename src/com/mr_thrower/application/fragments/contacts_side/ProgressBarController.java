package com.mr_thrower.application.fragments.contacts_side;

import com.mr_thrower.application.fragments.chat_side.MessageFragmentController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ProgressBar;
import javafx.scene.layout.AnchorPane;
import org.apache.log4j.Logger;

import java.io.IOException;

public class ProgressBarController extends AnchorPane {
    private static Logger log = Logger.getLogger(MessageFragmentController.class.getName());
    private ClassLoader loader;


//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private static final String FXML_PATH_PROGRESSBAR_FRAGMENT = "fragments/progressbar_fragment.fxml";

    private static final String STYLESHEET_PATH = "styles/progressbar_fragment.css";
    private static final String STYLE_PROGRESSBAR = "progressbar";


//======================================================================================================================
// FXML NODES
//======================================================================================================================

    @FXML private AnchorPane container;
    @FXML private ProgressBar progressbar;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public ProgressBarController() {
        loader = getClass().getClassLoader();

        FXMLLoader fxmlLoader = new FXMLLoader(loader.getResource(FXML_PATH_PROGRESSBAR_FRAGMENT));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        setStyles();
    }


//======================================================================================================================
// SETTERS
//======================================================================================================================

    public void setStyles() {
        getStylesheets().add(loader.getResource(STYLESHEET_PATH).toExternalForm());

        progressbar.getStyleClass().add(STYLE_PROGRESSBAR);
    }

    public void setProgress(double process) {
        progressbar.setProgress(process);
    }
}
