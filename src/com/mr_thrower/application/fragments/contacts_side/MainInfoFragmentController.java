package com.mr_thrower.application.fragments.contacts_side;

import com.mr_thrower.application.SceneSelector;
import com.mr_thrower.application.scenes.MainSceneController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import library.codes.ThrowModeCodes;
import org.apache.log4j.Logger;

import java.io.IOException;

public class MainInfoFragmentController extends AnchorPane {
    private static Logger log = Logger.getLogger(MainInfoFragmentController.class.getName());
    private ClassLoader loader;


//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private static final String FXML_PATH = "fragments/main_info_fragment.fxml";
    private static final String STYLESHEET_PATH = "styles/main_info_fragment.css";

    private static final String STYLE_TILE_WHITE = "tile_white";
    private static final String STYLE_TILE_WIFI = "tile_wifi";
    private static final String STYLE_TILE_BT = "tile_bt";
    private static final String STYLE_TILE_DIRECT = "tile_direct";

    private static final String STYLE_LABEL_WIFI = "label_wifi";
    private static final String STYLE_LABEL_BT = "label_bt";
    private static final String STYLE_LABEL_DIRECT = "label_direct";


    private static final String PATH_IMAGE_WINDOWS_WHITE = "images/windows_white_64.png";
    private static final String PATH_IMAGE_WINDOWS_BLACK = "images/windows_black_64.png";
    private static final String PATH_IMAGE_WIFI_BLACK = "images/wifi_black_32.png";
    private static final String PATH_IMAGE_BT_WHITE = "images/bt_white_32.png";
    private static final String PATH_IMAGE_DIRECT_WHITE = "images/direct_white_32.png";


//======================================================================================================================
// FXML NODES
//======================================================================================================================

    @FXML private Label nameLabel;
    @FXML private Label deviceNameLabel;
    @FXML private Label networkNameLabel;

    @FXML private ImageView osImageView;
    @FXML private ImageView modeImageView;

    @FXML private GridPane profileGridPane;
    @FXML private StackPane wifiImageStackPane;
    @FXML private StackPane wifiNameStackPane;


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private Byte mode;
    private MainSceneController wifiNetworkSC;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public MainInfoFragmentController(MainSceneController wifiNetworkSC, Byte mode) {
        loader = getClass().getClassLoader();
        this.wifiNetworkSC = wifiNetworkSC;
        this.mode = mode;

        FXMLLoader fxmlLoader = new FXMLLoader(loader.getResource(FXML_PATH));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        setImages(mode);
        setStyles(mode);
    }


//======================================================================================================================
// SETTERS
//======================================================================================================================

    public void setImages(Byte mode) {
        String pathOsImage = "";
        String pathModeImage = "";

        switch (mode) {
            case ThrowModeCodes.WIFI_NETWORK:
                pathOsImage = PATH_IMAGE_WINDOWS_BLACK;
                pathModeImage = PATH_IMAGE_WIFI_BLACK;
                break;
            case ThrowModeCodes.BLUETOOTH:
                pathOsImage = PATH_IMAGE_WINDOWS_BLACK;
                pathModeImage = PATH_IMAGE_BT_WHITE;
                break;
            case ThrowModeCodes.WIFI_DIRECT:
                pathOsImage = PATH_IMAGE_WINDOWS_BLACK;
                pathModeImage = PATH_IMAGE_DIRECT_WHITE;
                break;
        }

        Image osImage = new Image(loader.getResource(pathOsImage).toString());
        Image modeImage = new Image(loader.getResource(pathModeImage).toString());

        osImageView.setImage(osImage);
        modeImageView.setImage(modeImage);
    }

    public void setStyles(Byte mode) {
        getStylesheets().add(loader.getResource(STYLESHEET_PATH).toExternalForm());

        String styleTile = "";
        String styleLabel = "";

        switch (mode) {
            case ThrowModeCodes.WIFI_NETWORK:
                styleTile = STYLE_TILE_WIFI;
                styleLabel = STYLE_LABEL_WIFI;
                break;
            case ThrowModeCodes.BLUETOOTH:
                styleTile = STYLE_TILE_BT;
                styleLabel = STYLE_LABEL_BT;
                break;
            case ThrowModeCodes.WIFI_DIRECT:
                styleTile = STYLE_TILE_DIRECT;
                styleLabel = STYLE_LABEL_DIRECT;
                break;
        }


        profileGridPane.getStyleClass().add(STYLE_TILE_WHITE);
        wifiImageStackPane.getStyleClass().add(styleTile);
        wifiNameStackPane.getStyleClass().add(styleTile);

        nameLabel.getStyleClass().add(STYLE_LABEL_WIFI);
        deviceNameLabel.getStyleClass().add(STYLE_LABEL_WIFI);
        networkNameLabel.getStyleClass().add(styleLabel);
    }

    public void setWifiNetName(String wifiName) {
        networkNameLabel.setText(wifiName);
    }

    public void setName(String name) {
        nameLabel.setText(name);
    }

    public void setDeviceName(String deviceName) {
        deviceNameLabel.setText(deviceName);
    }


//======================================================================================================================
// CALLBACKS
//======================================================================================================================

    public void onWifiImageClicked() {
        SceneSelector.getInstance().setWelcomeScene();
    }

    public void onWifiNetworkClicked() {
        if(mode == ThrowModeCodes.WIFI_NETWORK) {
            SceneSelector.getInstance().showWifiChoiceWindow();
        }
    }
}
