package com.mr_thrower.application.fragments.contacts_side;

import com.mr_thrower.application.scenes.MainSceneController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import library.codes.OsCodes;
import library.objects.GreetingObject;
import library.wrappers.MacAddress;
import library.codes.ThrowModeCodes;
import org.apache.log4j.Logger;

import java.io.IOException;

public class ContactFragmentController extends AnchorPane {
    private static Logger log = Logger.getLogger(MainInfoFragmentController.class.getName());
    private ClassLoader loader;


//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    public static final int OS_WINDOWS = 1;
    public static final int OS_ANDROID = 2;

    public final static double CONTACTS_WIDTH_SHORT = 244;
    public final static double CONTACTS_WIDTH_LONG = 256;

    private static final String FXML_PATH_CONTACT_FRAGMENT = "fragments/contact_fragment.fxml";
    private static final String PATH_STYLESHEET_WIFI_SELECTED = "styles/contact_wifi_selected_fragment.css";
    private static final String PATH_STYLESHEET_BT_SELECTED = "styles/contact_bt_selected_fragment.css";
    private static final String PATH_STYLESHEET_DIRECT_SELECTED = "styles/contact_direct_selected_fragment.css";
    private static final String PATH_STYLESHEET_UNSELECTED = "styles/contact_unselected_fragment.css";


    private static final String STYLE_TILE = "tile";
    private static final String STYLE_LABEL = "label";
    private static final String STYLE_COUNTER_WIFI = "counter_wifi";
    private static final String STYLE_COUNTER_BT = "counter_bt";
    private static final String STYLE_COUNTER_DIRECT = "counter_direct";


    private static final String PATH_IMAGE_OS_WINDOWS_BLACK = "images/windows_black_64.png";
    private static final String PATH_IMAGE_OS_WINDOWS_WHITE = "images/windows_white_64.png";
    private static final String PATH_IMAGE_OS_ANDROID_BLACK = "images/android_black_64.png";
    private static final String PATH_IMAGE_OS_ANDROID_WHITE = "images/android_white_64.png";


//======================================================================================================================
// FXML NODES
//======================================================================================================================

    @FXML private GridPane contactGridPane;

    @FXML private ImageView osImageView;

    @FXML private Label contactNameLabel;
    @FXML private Label ipAddressLabel;
    @FXML private Label counterLabel;


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private MainSceneController wifiNetworkSC;

    private Byte mode;
    private GreetingObject greeting;
    private int counter;

//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public ContactFragmentController(GreetingObject greeting, Byte mode, MainSceneController wifiNetworkSC) {
        loader = getClass().getClassLoader();
        this.greeting = greeting;
        this.mode = mode;
        this.wifiNetworkSC = wifiNetworkSC;

        FXMLLoader fxmlLoader = new FXMLLoader(loader.getResource(FXML_PATH_CONTACT_FRAGMENT));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        contactNameLabel.setText(greeting.getName());
        ipAddressLabel.setText(greeting.getDevice());

        setImageOsUnselected(greeting.getOs());
        setStyles(mode, false);
        setCounterToZero();
    }


//======================================================================================================================
// SETTERS
//======================================================================================================================

    private void setImageOsSelected(Byte os, Byte mode) {
        String pathOsImage = "";

        switch(mode) {
            case ThrowModeCodes.WIFI_NETWORK:
                if(os == OsCodes.WINDOWS) {
                    pathOsImage = PATH_IMAGE_OS_WINDOWS_BLACK;
                } else if(os == OsCodes.ANDROID) {
                    pathOsImage = PATH_IMAGE_OS_ANDROID_BLACK;
                }
                break;

            case ThrowModeCodes.BLUETOOTH:
                if(os == OsCodes.WINDOWS) {
                    pathOsImage = PATH_IMAGE_OS_WINDOWS_WHITE;
                } else if(os == OsCodes.ANDROID) {
                    pathOsImage = PATH_IMAGE_OS_ANDROID_WHITE;
                }
                break;

            case ThrowModeCodes.WIFI_DIRECT:
                if(os == OsCodes.WINDOWS) {
                    pathOsImage = PATH_IMAGE_OS_WINDOWS_WHITE;
                } else if(os == OsCodes.ANDROID) {
                    pathOsImage = PATH_IMAGE_OS_ANDROID_WHITE;
                }
                break;
        }

        osImageView.setImage(new Image(loader.getResource(pathOsImage).toString()));
    }

    private void setImageOsUnselected(Byte os) {
        String pathOsImage = "";

        if(os == OsCodes.WINDOWS) {
            pathOsImage = PATH_IMAGE_OS_WINDOWS_BLACK;
        } else if(os == OsCodes.ANDROID) {
            pathOsImage = PATH_IMAGE_OS_ANDROID_BLACK;
        }

        osImageView.setImage(new Image(loader.getResource(pathOsImage).toString()));
    }

    private void setStyles(Byte mode, boolean selected) {
        String pathStylesheet = "";

        switch (mode) {
            case ThrowModeCodes.WIFI_NETWORK:
                pathStylesheet = PATH_STYLESHEET_WIFI_SELECTED;
                break;
            case ThrowModeCodes.BLUETOOTH:
                pathStylesheet = PATH_STYLESHEET_BT_SELECTED;
                break;
            case ThrowModeCodes.WIFI_DIRECT:
                pathStylesheet = PATH_STYLESHEET_DIRECT_SELECTED;
                break;
        }

        if(!selected) {
            pathStylesheet = PATH_STYLESHEET_UNSELECTED;
        }
        if(getStylesheets().size() != 0)
            getStylesheets().remove(0);
        getStylesheets().add(loader.getResource(pathStylesheet).toExternalForm());

        contactGridPane.getStyleClass().add(STYLE_TILE);
        contactNameLabel.getStylesheets().add(STYLE_LABEL);
        ipAddressLabel.getStylesheets().add(STYLE_LABEL);

        if(!selected) {
            switch (mode) {
                case ThrowModeCodes.WIFI_NETWORK:
                    counterLabel.getStyleClass().add(STYLE_COUNTER_WIFI);
                    break;
                case ThrowModeCodes.BLUETOOTH:
                    counterLabel.getStyleClass().add(STYLE_COUNTER_BT);
                    break;
                case ThrowModeCodes.WIFI_DIRECT:
                    counterLabel.getStyleClass().add(STYLE_COUNTER_DIRECT);
                    break;
            }
        }
    }


//======================================================================================================================
// GUI CALLS
//======================================================================================================================

    public void setSelected() {
        setImageOsSelected(greeting.getOs(), mode);
        setStyles(mode, true);
    }

    public void setUnselected() {
        setImageOsUnselected(greeting.getOs());
        setStyles(mode, false);
    }

    public void setCounterToZero() {
        counterLabel.setVisible(false);
        counter = 0;
    }

    public void incrementCounter() {
        if(!counterLabel.isVisible()) {
            counterLabel.setVisible(true);
        }

        counter++;
        counterLabel.setText(String.valueOf(counter));
    }


//======================================================================================================================
// GETTERS
//======================================================================================================================

    public MacAddress getMacAddress() {
        return greeting.getMacAddress();
    }

    public String getContactName() { return contactNameLabel.getText(); }


//======================================================================================================================
// CALLBACKS
//======================================================================================================================

    @FXML
    public void onContactClicked() {
        wifiNetworkSC.onContactSelected(this);
    }


}
