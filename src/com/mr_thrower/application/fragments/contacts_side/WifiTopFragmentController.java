package com.mr_thrower.application.fragments.contacts_side;

import com.mr_thrower.application.scenes.WelcomeSceneController;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import org.apache.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

public class WifiTopFragmentController implements Initializable {
    private static Logger log = Logger.getLogger(WelcomeSceneController.class.getName());
    private ClassLoader loader;


    @FXML private ImageView logo;
    @FXML private StackPane stack;
    @FXML private Pane tile;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loader = getClass().getClassLoader();

        Image image = new Image(loader.getResource("images/icon_bt_dark.png").toString());

        logo.setImage(image);
        logo.setPreserveRatio(true);

        stack.widthProperty().addListener(e -> {
            double fitWidth = stack.widthProperty().get();
            logo.setFitWidth(fitWidth);
        });

        stack.heightProperty().addListener(e -> {
            double fitHeight = stack.heightProperty().get();
            logo.setFitHeight(fitHeight);
        });



    }
}
