package com.mr_thrower.application.fragments.wifi_chooser;

import com.mr_thrower.application.fragments.wifi_chooser.WifiInfoFragmentController;
import com.mr_thrower.application.monitors.NetExpert;
import com.mr_thrower.application.scenes.WifiSelectSceneController;
import com.mr_thrower.application.fragments.chat_side.ChatFragmentController;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import library.wrappers.WifiNetwork;
import org.apache.log4j.Logger;

import java.io.IOException;

public class WifiNetworkFragmentController extends AnchorPane {
    private static Logger log = Logger.getLogger(ChatFragmentController.class.getName());
    private ClassLoader loader;


//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private static final String FXML_PATH_WIFI_NETWORK_FRAGMENT = "fragments/wifi_network_fragment.fxml";

    private static final String STYLESHEET_PATH = "styles/wifi_network_fragment.css";
    private static final String STYLE_TILE_YELLOW = "tile-yellow";
    private static final String STYLE_TILE_DARK = "tile-dark";
    private static final String STYLE_LABEL_YELLOW = "label-yellow";
    private static final String STYLE_LABEL_DARK = "label-dark";

    private static final String IMAGE_PATH_CONNECT = "images/double_arrow_24.png";
    private static final String IMAGE_PATH_WIFI = "images/wifi_black_24.png";



//======================================================================================================================
// FXML NODES
//======================================================================================================================

    @FXML private AnchorPane container;
    @FXML private VBox vbox;
    @FXML private StackPane stackpane;
    @FXML private Label ssidLabel;
    @FXML private ImageView connectImageView;


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private WifiSelectSceneController wifiSelectSC;
    private WifiNetwork wifiNetwork;
    private int stagnation;
    private WifiInfoFragmentController wifiInfoFC;

    private boolean isCurrentWifiNetwork;
    private boolean isSelected;
    private boolean connectionSuccess;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public WifiNetworkFragmentController(WifiSelectSceneController wifiSelectSC, WifiNetwork wifiNetwork) {
        loader = getClass().getClassLoader();
        this.wifiSelectSC = wifiSelectSC;
        this.wifiNetwork = wifiNetwork;

        isCurrentWifiNetwork = false;
        wifiInfoFC = null;
        isSelected = false;
        stagnation = 0;
        connectionSuccess = true;

        FXMLLoader fxmlLoader = new FXMLLoader(loader.getResource(FXML_PATH_WIFI_NETWORK_FRAGMENT));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        disableImage();
        setCommonStyles();

        ssidLabel.setText(wifiNetwork.getSsid());
    }


//======================================================================================================================
// SETTERS
//======================================================================================================================

    private void setCommonStyles() {
        getStylesheets().add(loader.getResource(STYLESHEET_PATH).toExternalForm());

        container.getStyleClass().setAll(STYLE_TILE_YELLOW);
        ssidLabel.getStyleClass().setAll(STYLE_LABEL_DARK);
    }

    public void markCurrentWifiNetwork() {
        isCurrentWifiNetwork = true;

        Image wifiImage = new Image(loader.getResource(IMAGE_PATH_WIFI).toString());
        connectImageView.setImage(wifiImage);
    }

    public void uncheckCurrentWifiNetwork() {
        isCurrentWifiNetwork = false;

        //setImages();
    }

    private void setSelectedStyles() {
        getStylesheets().add(loader.getResource(STYLESHEET_PATH).toExternalForm());

        container.getStyleClass().setAll(STYLE_TILE_DARK);
        ssidLabel.getStyleClass().setAll(STYLE_LABEL_YELLOW);
    }

    private void setImages() {
        Image connectImage = new Image(loader.getResource(IMAGE_PATH_CONNECT).toString());
        connectImageView.setImage(connectImage);
        stackpane.setDisable(false);
    }

    public void disableImage() {
        connectImageView.setImage(null);
        stackpane.setDisable(true);
    }


    public void disableInfo() {
        log.info("DISABLE");
        vbox.getChildren().remove(wifiInfoFC);
        wifiInfoFC = null;
        this.setPrefHeight(50);

        disableImage();
        setCommonStyles();
    }


//======================================================================================================================
// GETTERS
//======================================================================================================================

    public WifiNetwork getWifiNetwork() {
        return wifiNetwork;
    }

    public int getStagnation() {
        return stagnation;
    }

    public void setStagnation(int num) {
        stagnation = num;
    }

    public boolean isWifiInfoShow() {
        return (wifiInfoFC != null);
    }


//======================================================================================================================
// CALLBACKS
//======================================================================================================================

    @FXML
    public void onNetworkClicked() {
        if(!isCurrentWifiNetwork) {
            if (!isSelected) {
                log.info("clicked");
                isSelected = true;

                if (!NetExpert.checkProfile(wifiNetwork.getSsid())) {
                    wifiInfoFC = new WifiInfoFragmentController();
                    vbox.getChildren().add(wifiInfoFC);
                    this.setPrefHeight(90);
                    disableImage();
                }

                if(connectionSuccess) {
                    setImages();
                }
                setSelectedStyles();

                wifiSelectSC.onWifiInfoShow(this);


            } else {
                isSelected = false;

                disableImage();
                setCommonStyles();

                vbox.getChildren().remove(wifiInfoFC);
                wifiSelectSC.onWifiInfoShow(null);
            }
        }
    }

    @FXML
    public void onConnectClicked() {
        if(!isCurrentWifiNetwork) {
            // folding neutralization
            onNetworkClicked();

            if(NetExpert.connectToProfile(wifiNetwork.getSsid())) {
                // close scene
                wifiSelectSC.onWifiChanged();
            } else {
                // show error
                connectionSuccess = false;
                disableImage();
            }
        }
    }




}
