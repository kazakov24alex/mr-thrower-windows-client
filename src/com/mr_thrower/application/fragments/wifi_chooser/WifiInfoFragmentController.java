package com.mr_thrower.application.fragments.wifi_chooser;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import org.apache.log4j.Logger;

import java.io.IOException;


public class WifiInfoFragmentController extends AnchorPane {
    private static Logger log = Logger.getLogger(WifiInfoFragmentController.class.getName());
    private ClassLoader loader;


//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private static final String FXML_PATH_WIFI_PASSWORD_FRAGMENT = "fragments/wifi_info_fragment.fxml";

    private static final String STYLESHEET_PATH = "styles/wifi_info_fragment.css";
    private static final String STYLE_INFO_LABLE = "info-text";


//======================================================================================================================
// FXML NODES
//======================================================================================================================

    @FXML private AnchorPane container;
    @FXML private Label infoLabel;

//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public WifiInfoFragmentController() {
        loader = getClass().getClassLoader();

        FXMLLoader fxmlLoader = new FXMLLoader(loader.getResource(FXML_PATH_WIFI_PASSWORD_FRAGMENT));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        setStyles();
        setLocalization();
    }


//======================================================================================================================
// SETTERS
//======================================================================================================================

    private void setStyles() {
        getStylesheets().add(loader.getResource(STYLESHEET_PATH).toExternalForm());

        infoLabel.getStyleClass().add(STYLE_INFO_LABLE);
    }

    private void setLocalization() {

    }

    public void setInfoText(String info) {
        infoLabel.setText(info);
    }


}
