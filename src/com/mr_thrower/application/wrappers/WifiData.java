package com.mr_thrower.application.wrappers;

import library.wrappers.WifiNetwork;

import java.util.Vector;

public class WifiData {

    private Vector<WifiNetwork> wifiNetworkVector;
    private String currentSSID;


    public WifiData(Vector<WifiNetwork> wifiNetworkVector, String currentSSID) {
        this.wifiNetworkVector = wifiNetworkVector;
        this.currentSSID = currentSSID;
    }


    public Vector<WifiNetwork> getWifiNetworks() {
        return wifiNetworkVector;
    }

    public String getCurrentSSID() {
        return currentSSID;
    }


}
