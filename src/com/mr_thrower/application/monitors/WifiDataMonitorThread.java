package com.mr_thrower.application.monitors;

import com.mr_thrower.application.monitors.interfaces.IWifiDataMonitorListener;
import com.mr_thrower.application.wrappers.WifiData;
import javafx.application.Platform;
import library.wrappers.WifiNetwork;
import org.apache.log4j.Logger;
import org.greenrobot.eventbus.EventBus;

import java.util.Vector;


public class WifiDataMonitorThread extends Thread {
    private static Logger log = Logger.getLogger(WifiDataMonitorThread.class.getName());


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private boolean isRunning;
    private int timeout;
    private Vector<IWifiDataMonitorListener> wifiDataListeners;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public WifiDataMonitorThread(int timeout) {
        this.timeout = timeout;
        isRunning = false;
        wifiDataListeners = new Vector<IWifiDataMonitorListener>();

        // daemon-thread setting
        setDaemon(true);

        // determining the default priority for the thread
        setPriority(NORM_PRIORITY);
    }


//======================================================================================================================
// OPERATING CYCLE
//======================================================================================================================

    @Override
    public void run() {
        isRunning = true;
        log.info("thread started");

        while(isRunning) {
            Platform.runLater(() -> {
                Vector<WifiNetwork> networks = NetExpert.getWifiNetworks();
                String currentSSID = NetExpert.getCurrentSSID();

                EventBus.getDefault().post(new WifiData(networks, currentSSID));
            });

            try {
                sleep(timeout);
            } catch (InterruptedException e)  {
                if(isRunning) {
                    log.error(e.getMessage());
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void interrupt() {
        isRunning = false;
        log.info("thread stopped");

        super.interrupt();
    }


}
