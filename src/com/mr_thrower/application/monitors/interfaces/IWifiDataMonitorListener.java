package com.mr_thrower.application.monitors.interfaces;

import com.mr_thrower.application.wrappers.WifiData;


public interface IWifiDataMonitorListener {

    public void onWifiDataUpdate(WifiData wifiData);

}
