package com.mr_thrower.application.monitors;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.util.Vector;

import library.exceptions.EncodingException;
import library.wrappers.IpAddress;
import library.wrappers.MacAddress;
import library.wrappers.WifiNetwork;
import org.apache.log4j.Logger;


public class NetExpert {
    private static Logger log = Logger.getLogger(NetExpert.class.getName());

    private static String ENGLISH_FORMAT = "chcp 437";
    private static String NETSH_WLAN_SHOW_INTERFACE = "netsh wlan show interface";
    private static String NETSH_WLAN_SHOW_WIRELESSCAPABILITIES = "netsh wlan show wirelesscapabilities";
    private static String NETSH_WLAN_SHOW_NETWORKS = "netsh wlan show networks";
    private static String NETSH_WLAN_SHOW_PROFILES = "netsh wlan show profile";
    private static String NETSH_WLAN_CONNECT = "netsh wlan connect name=";
    private static String ECHO_USERNAME = "echo %username%";

    private static String RESPONSE_CONNECT_SUCCESS = "Connection request was completed successfully";

    public static String STATE_WIFI_NOT_CONNECTED = "NOT CONNECTED";


    private static BufferedReader cmdExecution(String command) {
        ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", command);
        builder.redirectErrorStream(true);

        try {
            Process process = builder.start();
            return new BufferedReader(new InputStreamReader(process.getInputStream()));

        } catch (IOException e) {
            log.error( "Exception", e);
            return null;
        }
    }


    public static MacAddress getMacAddress() {
        byte[] mac = null;

        try {
            InetAddress ip = InetAddress.getLocalHost();
            NetworkInterface network = NetworkInterface.getByInetAddress(ip);
            mac = network.getHardwareAddress();
        } catch (IOException e) {
            log.error("NetExpert: " + e.getMessage());
            e.printStackTrace();
        }

        try {
            return new MacAddress(mac);
        } catch (EncodingException e) {
            return null;
        }
    }

    public static IpAddress getIpAddress() {
        IpAddress ipAddress = null;

        try {
            InetAddress ip = InetAddress.getLocalHost();
            ipAddress = new IpAddress(ip.getAddress());

        } catch (EncodingException e) {
            log.error("NetExpert: " + e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            log.error("NetExpert: " + e.getMessage());
            e.printStackTrace();
        }

        return ipAddress;
    }


    public static int checkWirelessDevice() {
        int numWirelessDevices = 0;

        BufferedReader response = cmdExecution(ENGLISH_FORMAT + " & " + NETSH_WLAN_SHOW_INTERFACE);

        String line = " ";
        while (true) {
            try {
                line = response.readLine();
            } catch (IOException e) {
                log.error( "Exception", e);
                break;
            }

            if (line == null) {
                break;
            }

            if(line.contains("Name")) {
                numWirelessDevices++;
                log.info("NETSH: Wireless Device found:");
                log.info(line);
            }

            if(line.contains("Description")) {
                log.info(line);
            }
        }

        try {
            response.close();
        } catch (IOException e) {
            log.error( "Exception", e);
        }

        return numWirelessDevices;
    }


    public static boolean checkWifiDirectDevice() {
        BufferedReader response = cmdExecution(ENGLISH_FORMAT + " & " + NETSH_WLAN_SHOW_WIRELESSCAPABILITIES);

        String line = " ";
        while (true) {
            try {
                line = response.readLine();
            } catch (IOException e) {
                log.error( "Exception", e);
                break;
            }

            if (line == null) {
                break;
            }

            if(line.contains("Wi-Fi Direct Device") && !line.contains("Not Supported")) {
                log.info("NETSH: Wi-Fi Direct Device supported");

                try {
                    response.close();
                } catch (IOException e) {
                    log.error( "Exception", e);
                }

                return true;
            }

        }

        log.info("NETSH: Wi-Fi Direct Device not supported");
        return false;
    }


    public static boolean checkWifiDirectGO() {
        BufferedReader response = cmdExecution(ENGLISH_FORMAT + " & " + NETSH_WLAN_SHOW_WIRELESSCAPABILITIES);

        String line = " ";
        while (true) {
            try {
                line = response.readLine();
            } catch (IOException e) {
                log.error( "Exception", e);
                break;
            }

            if (line == null) {
                break;
            }

            if(line.contains("Wi-Fi Direct GO") && !line.contains("Not Supported")) {
                log.info("NETSH: Wi-Fi Direct GO supported");

                try {
                    response.close();
                } catch (IOException e) {
                    log.error( "Exception", e);
                }

                return true;
            }

        }

        log.info("NETSH: Wi-Fi Direct GO not supported");
        return false;
    }


    public static boolean checkWifiDirectClient() {
        BufferedReader response = cmdExecution(ENGLISH_FORMAT + " & " + NETSH_WLAN_SHOW_WIRELESSCAPABILITIES);

        String line = " ";
        while (true) {
            try {
                line = response.readLine();
            } catch (IOException e) {
                log.error( "Exception", e);
                break;
            }

            if (line == null) {
                break;
            }

            if(line.contains("Wi-Fi Direct Client") && !line.contains("Not Supported")) {
                log.info("NETSH: Wi-Fi Direct Client supported");

                try {
                    response.close();
                } catch (IOException e) {
                    log.error( "Exception", e);
                }

                return true;
            }

        }

        log.info("NETSH: Wi-Fi Direct Client not supported");
        return false;
    }


    public static String getCurrentSSID() {
        BufferedReader response = cmdExecution(ENGLISH_FORMAT + " & " + NETSH_WLAN_SHOW_INTERFACE);

        String line = " ";
        while (true) {
            try {
                line = response.readLine();
            } catch (IOException e) {
                log.error( "Exception", e);
                break;
            }

            if (line == null) {
                break;
            }

            if(line.contains("Profile") && !line.contains("Connection mode")) {
                String ssid = line.substring(line.lastIndexOf(":") + 2, line.length()-1);
                //log.info("Current SSID: " + ssid);
                return ssid;
            }
        }

        log.info("WIFI NOT CONNECTED");
        return STATE_WIFI_NOT_CONNECTED;
    }


    public static String getUsername() {
        BufferedReader response = cmdExecution(ECHO_USERNAME);

        String line = " ";
        while (true) {
            try {
                line = response.readLine();
            } catch (IOException e) {
                log.error( "Exception", e);
                break;
            }

            if (line == null) {
                break;
            }

            log.info("Username: " + line);
            return line;
        }

        log.info("USERNAME NOT FOUND");
        return "USERNAME_NOT_FOUND";
    }

    public static Vector<WifiNetwork> getWifiNetworks() {
        BufferedReader response = cmdExecution(ENGLISH_FORMAT + " & " + NETSH_WLAN_SHOW_NETWORKS);
        Vector<WifiNetwork> wifiNetworkVector = new Vector<WifiNetwork>();
        String line = " ";
        while (true) {
            try {
                line = response.readLine();

                if (line == null) {
                    break;
                }

                if(line.contains("SSID")) {
                    String ssid = line.substring(line.lastIndexOf(":") + 2, line.length());
                    line = response.readLine();
                    String networkType = line.substring(line.lastIndexOf(":") + 2, line.length());
                    line = response.readLine();
                    String authentication = line.substring(line.lastIndexOf(":") + 2, line.length());
                    line = response.readLine();
                    String encryption = line.substring(line.lastIndexOf(":") + 2, line.length());

                    WifiNetwork wifiNetwork = new WifiNetwork(ssid, networkType, authentication, encryption);
                    wifiNetworkVector.add(wifiNetwork);

                    //log.info("Found: " + wifiNetwork.toString());
                }

            } catch (IOException e) {
                log.error( "Exception", e);
                break;
            }
        }

        return wifiNetworkVector;
    }

    public static boolean checkProfile(String profileName) {
        BufferedReader response = cmdExecution(ENGLISH_FORMAT + " & " + NETSH_WLAN_SHOW_PROFILES);

        String line = " ";
        while (true) {
            try {
                line = response.readLine();

                if (line == null) {
                    break;
                }

                if(line.contains(profileName)) {
                    return true;
                }

            } catch (IOException e) {
                log.error( "Exception", e);
                break;
            }
        }

        return false;
    }

    public static boolean connectToProfile(String profileName) {
        BufferedReader response = cmdExecution(ENGLISH_FORMAT + " & " + NETSH_WLAN_CONNECT + profileName);

        String line = " ";
        while (true) {
            try {
                line = response.readLine();

                if (line == null) {
                    break;
                }

                if(line.contains(RESPONSE_CONNECT_SUCCESS)) {
                    log.info("JOINED the \"" + profileName + "\" WiFi network");
                    return true;
                }

            } catch (IOException e) {
                log.error( "Exception", e);
                break;
            }
        }

        return false;
    }
}
