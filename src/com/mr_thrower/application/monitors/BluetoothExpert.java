package com.mr_thrower.application.monitors;

import library.exceptions.EncodingException;
import library.wrappers.MacAddress;

public class BluetoothExpert {

    public static MacAddress extractMacFromBtURL(String btUrl) {
        // btspp://1436C6CFA8F9:5;authenticate=false;encrypt=false;master=false

        String solidMacStr = "";
        for(int i = 8; i < 20; i++) {
            solidMacStr += btUrl.charAt(i);
        }

        try {
            return MacAddress.getFromSolidFormat(solidMacStr);
        } catch (EncodingException e) {
            return null;
        }
    }

}
