package com.mr_thrower.application.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;


public class AccessToSQLite {
    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AccessToSQLite.class.getName());

    private String sConnect;
    private Connection connection = null;


    public AccessToSQLite(String sConnect) {
        this.sConnect = sConnect;
    }

    public boolean getAccess(){
        return  connectToSQLite();
    }

    public Statement getStatement() throws SQLException {
        return connection.createStatement();
    }

    private boolean connectToSQLite() {
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            log.info("connectToSQLite: SQLite JDBC driver not found");
            return false;
        }
        try {
            connection = DriverManager.getConnection("jdbc:sqlite:"+sConnect);
            log.info("connectToSQLite: connection to the database via JDBC driver is established");
        } catch (SQLException e) {
            log.info("connectToSQLite: error connecting to the database via JDBC driver");
            return false;
        }
        return true;
    }

    public boolean closeAccess(){
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public Connection getConnection() {
        return connection;
    }


}