package com.mr_thrower.application.database;



import com.mr_thrower.application.monitors.NetExpert;
import library.codes.*;
import library.database.Database;
import library.database.RecordReport;
import library.database.tables.*;
import library.database.views.DbViewMessage;
import library.database.views.DbViewThrow;
import library.exceptions.EncodingException;
import library.objects.GreetingObject;
import library.objects.MessageObject;
import library.objects.ThrowFileObject;
import library.wrappers.MD5;
import library.wrappers.MacAddress;
import library.wrappers.MessageWrapper;
import library.wrappers.ThrowWrapper;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;


public class DbManager {
    private static org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AccessToSQLite.class.getName());

//======================================================================================================================
// SINGLETON
//======================================================================================================================

    private static DbManager instance;

    public static synchronized DbManager getInstance() {
        if (instance == null) {
            instance = new DbManager();
        }
        return instance;
    }


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private AccessToSQLite accessToSQLite;
    private Statement statement;


//======================================================================================================================
// INITIALIZATION
//======================================================================================================================

    public DbManager() { }

    public void initAccessToDB() {
        if(accessToSQLite != null)
            return;


        accessToSQLite = new AccessToSQLite(getFilePathInRoot(Database.DATABASE_NAME));
        if (accessToSQLite.getAccess()) {
            log.info("connection to the database is established");
        } else {
            log.info("database access error");
            System.exit(1);
        }

        try {
            statement = accessToSQLite.getStatement();
        } catch (SQLException e) {
            closeAccessToDB();

            log.error(e.getMessage());
            e.printStackTrace();
        }

        execMultipleStatements(Database.tableInitQuery());
        execMultipleStatements(Database.viewInitQuery());
        execMultipleStatements(Database.fillTablesQuery());

    }

    public void closeAccessToDB() {
        accessToSQLite.closeAccess();
        log.info("access to the database is disabled");
    }


// =====================================================================================================================
// UTIL METHODS
// =====================================================================================================================

    private static String getFilePathInRoot(String strFileName) {
        String iStr = new File(".").getAbsolutePath();
        iStr = iStr.substring(0, iStr.length() - 1);
        return iStr+strFileName;
    }

    private void execMultipleStatements(ArrayList<String> statements) {
        for(String st : statements) {
            execStatement(st);
        }
    }

    private void execStatement(String strSQL) {
        try {
            statement.execute(strSQL);
        } catch (SQLException e) {
            log.info("Ошибка statement.execute: " + e.getMessage());
        }
    }

    private ArrayList<RecordReport> execSelect(String query) {
        ArrayList<String> sqlQuery = new ArrayList<>();
        sqlQuery.add(query);

        ResultSet resultSQLquery = execSQLquery(sqlQuery);
        return getReportOnTheRequest(resultSQLquery);
    }

    private ResultSet execSQLquery(ArrayList<String> sqlQuery) {
        ResultSet resultSQLquery;
        String strSQL = "";
        for (String iStr : sqlQuery) {
            strSQL += iStr + "\n";
        }

        try {
            resultSQLquery = statement != null ? statement.executeQuery(strSQL) : null;
        } catch (SQLException e) {
            log.info("execSQLquery: " + e.getMessage());
            return null;
        }
        return resultSQLquery;
    }

    private ArrayList<RecordReport> getReportOnTheRequest(ResultSet resultSQLquery) {
        ArrayList<RecordReport> listReport = new ArrayList<>();
        if (resultSQLquery == null) return listReport;
        int nColumnsCount;
        try {
            nColumnsCount = resultSQLquery.getMetaData().getColumnCount();
        } catch (SQLException e) {
            log.info("getReportOnTheRequest: " + e.getMessage());
            return listReport;
        }
        if (nColumnsCount == 0) return listReport;
        try {
            while (resultSQLquery.next()) {
                RecordReport recordReport = new RecordReport(nColumnsCount);
                for (int i = 1; i < nColumnsCount + 1; i++) {
                    Object iObj = resultSQLquery.getObject(i);
                    if (iObj != null) {
                        recordReport.putItemField(i, iObj.toString().trim());
                    } else {
                        recordReport.putItemField(i, "");
                    }
                }
                listReport.add(recordReport);
            }
        } catch (SQLException e) {
            log.info("getReportOnTheRequest: " + e.getMessage());
        }
        return listReport;
    }


// =====================================================================================================================
// FUNCTIONS OF RECORDING TO THE DATABASE
// =====================================================================================================================

    public void insertContact(GreetingObject greeting) {
        execStatement(DbTableContact.insertQuery(greeting));
    }

    public void insertMessage(MessageWrapper message) {
        int contactId = getContactIdByMac(message.getAddress());
        execStatement(DbTableMessage.insertQuery(contactId, message));
    }

    public void insertThrow(ThrowWrapper throwWr, Byte state) {
        throwWr.setState(state);

        int contactId = getContactIdByMac(throwWr.getAddress());
        execStatement(DbTableThrow.insertQuery(contactId, throwWr));
    }


// =====================================================================================================================
// FUNCTIONS OF SELECTING FROM THE DATABASE
// =====================================================================================================================

    private int getContactIdByMac(MacAddress macAddress) {
        ArrayList<RecordReport> reports = execSelect(DbTableContact.selectByMacQuery(macAddress));
        return DbTableContact.decodeContactId(reports.get(0));
    }

    public ArrayList<GreetingObject> selectAllContacts() {
        ArrayList<RecordReport> reports = execSelect(DbTableContact.selectAllQuery());

        ArrayList<GreetingObject> contacts = new ArrayList<>();
        for(RecordReport report : reports) {
            try {
                contacts.add(DbTableContact.decodeRecordReport(report));
            } catch (Exception e) { }
        }

        return contacts;
    }

    public ArrayList<MessageWrapper> selectMessagesByMac(MacAddress macAddress) {
        ArrayList<RecordReport> reports = execSelect(DbViewMessage.selectByMac(macAddress));

        ArrayList<MessageWrapper> messages = new ArrayList<>();
        for(RecordReport report : reports) {
            try {
                messages.add(DbViewMessage.decodeRecordReport(report));
            } catch (Exception e) { }
        }

        return messages;
    }

    public ArrayList<ThrowWrapper> selectThrowsByMac(MacAddress macAddress) {
        ArrayList<RecordReport> reports = execSelect(DbViewThrow.selectByMac(macAddress));

        ArrayList<ThrowWrapper> _throws = new ArrayList<>();
        for(RecordReport report : reports) {
            try {
                _throws.add(DbViewThrow.decodeRecordReport(report));
            } catch (Exception e) { }
        }

        return _throws;
    }

    public ThrowWrapper selectThrowsByTimeID(MacAddress macAddress, Date timeID) {
        ArrayList<RecordReport> reports = execSelect(DbViewThrow.selectByMac(macAddress));

        ArrayList<ThrowWrapper> _throws = new ArrayList<>();
        for(RecordReport report : reports) {
            try {
                ThrowWrapper tFile = DbViewThrow.decodeRecordReport(report);
                if(tFile.getTimeID().getTime() == timeID.getTime())
                    return tFile;
            } catch (Exception e) { }
        }

        return null;
    }



// =====================================================================================================================
// FUNCTIONS OF UPDATING IN THE DATABASE
// =====================================================================================================================

    public void updateContactName(MacAddress macAddress, String name) {
        execStatement(DbTableContact.updateNameQuery(macAddress, name));
    }

    public void updateMessageState(MacAddress macAddress, Date timeId, Byte state) {
        int contactId = getContactIdByMac(macAddress);
        execStatement(DbTableMessage.updateStateQuery(contactId, timeId, state));
    }

    public void updateThrowState(MacAddress macAddress, Date timeId, Byte state) {
        int contactId = getContactIdByMac(macAddress);
        execStatement(DbTableThrow.updateStateQuery(contactId, timeId, state));
    }

    public void updateThrowPath(MacAddress macAddress, Date timeId, String path) {
        int contactId = getContactIdByMac(macAddress);
        execStatement(DbTableThrow.updatePathQuery(contactId, timeId, path));
    }


// =====================================================================================================================
// FUNCTIONS OF DELETING FROM THE DATABASE
// =====================================================================================================================
    public void deleteContact(MacAddress macAddress) {
        deleteMessagesOfContact(macAddress);
        deleteThrowsOfContact(macAddress);

        execStatement(DbTableContact.deleteContactQuery(macAddress));
    }

    public void deleteMessage(MacAddress macAddress, Date timeID) {
        int contactId = getContactIdByMac(macAddress);
        execStatement(DbTableMessage.deleteByTimeIdQuery(contactId, timeID));
    }

    public void deleteThrow(MacAddress macAddress, Date timeID) {
        int contactId = getContactIdByMac(macAddress);
        execStatement(DbTableThrow.deleteByTimeIdQuery(contactId, timeID));
    }


    public void deleteMessagesOfContact(MacAddress macAddress) {
        int contactId = getContactIdByMac(macAddress);
        execStatement(DbTableMessage.deleteByContactIdQuery(contactId));
    }

    public void deleteThrowsOfContact(MacAddress macAddress) {
        int contactId = getContactIdByMac(macAddress);
        execStatement(DbTableThrow.deleteByContactIdQuery(contactId));
    }


    private void clearAllContacts() {
        execStatement(DbTableContact.deleteAllQuery());
    }

    private void clearAllThrows() {
        execStatement(DbTableThrow.deleteAllQuery());
    }

    private void clearAllMessages() {
        execStatement(DbTableMessage.deleteAllQuery());
    }

    public void clearDatabase() {
        clearAllMessages();
        clearAllThrows();
        clearAllContacts();
    }




    // TODO tmp
    public GreetingObject getProfile() {
        return new GreetingObject(
                NetExpert.getMacAddress(),
                NetExpert.getIpAddress(),
                "ASUS",
                "NOTEBOOK",
                OsCodes.WINDOWS
        );
    }
}