package com.mr_thrower.application;

import javafx.application.Application;
import javafx.stage.Stage;
import org.apache.log4j.Logger;


public class AppGUI extends Application {
    private static Logger log = Logger.getLogger(AppGUI.class.getName());



    @Override
    public void start(Stage primaryStage) throws Exception {
        SceneSelector.getInstance().setPrimaryStage(primaryStage);
        SceneSelector.getInstance().setWelcomeScene();
    }


    public static void main(String[] args) {
        launch(args);
    }
}
