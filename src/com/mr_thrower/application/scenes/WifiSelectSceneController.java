package com.mr_thrower.application.scenes;

import com.mr_thrower.application.SceneSelector;
import com.mr_thrower.application.monitors.interfaces.IWifiDataMonitorListener;
import com.mr_thrower.application.fragments.wifi_chooser.WifiNetworkFragmentController;
import com.mr_thrower.application.wrappers.WifiData;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import library.wrappers.WifiNetwork;
import org.apache.log4j.Logger;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

public class WifiSelectSceneController implements Initializable, IWifiDataMonitorListener {
    private static Logger log = Logger.getLogger(WifiSelectSceneController.class.getName());
    private ClassLoader loader;


//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private static final String STYLESHEET_PATH = "styles/wifi_select_scene.css";
    private static final String STYLE_BACKGROUND = "background";
    private static final String STYLE_SCROLLER = "scroller";
    private static final String STYLE_WIFI_SEARCH_LABEL = "wifi-search-label";

    private static final int HORIZONTAL_RESOLUTION_MINIMAL = 254;
    private static final int HORIZONTAL_RESOLUTION_MAXIMUM = 266;
    private static final int SCROLLPANE_NUM_VISIBLE = 7;

    private static final int WIFi_MAX_STAGNATION_NUM = 5;


//======================================================================================================================
// FXML NODES
//======================================================================================================================

    @FXML private AnchorPane container;
    @FXML private ScrollPane scrollpane;
    @FXML private VBox vbox;
    @FXML private Label wifiSearchLabel;


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private Vector<WifiNetworkFragmentController> wifiNetworkFCVector;
    private WifiNetworkFragmentController lastWifiFragmentWithInfo;
    private String lastCurrentWifiNetwork;


//======================================================================================================================
// INITIALIZE
//======================================================================================================================

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loader = getClass().getClassLoader();
        wifiNetworkFCVector = new Vector<WifiNetworkFragmentController>();
        lastWifiFragmentWithInfo = null;
        lastCurrentWifiNetwork = "NOT";

        setStyle();

        // EventBus register
        EventBus.getDefault().register(this);
    }


//======================================================================================================================
// CALLBACKS
//======================================================================================================================

    public void onWifiChanged() {
        SceneSelector.getInstance().closeSecondScene();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    @Override
    public void onWifiDataUpdate(WifiData wifiData) {
        Platform.runLater(() -> {
            log.info("Wifi Networks list UPDATED");

            if (wifiData.getWifiNetworks().size() == 0)
                wifiSearchLabel.setVisible(true);
            else
                wifiSearchLabel.setVisible(false);

            updateWifiNetworks(wifiData.getWifiNetworks());
            checkCurrentWifiNetwork(wifiData.getCurrentSSID());
        });
    }

    public void onWifiInfoShow(WifiNetworkFragmentController wifiNetworkFC) {
        // disable last WifiInfoFragment
        if(lastWifiFragmentWithInfo != null) {
            lastWifiFragmentWithInfo.disableInfo();
        }

        // new InfoFragment appropriation
        lastWifiFragmentWithInfo = wifiNetworkFC;

        // fit container height
        if(wifiNetworkFC != null) {
            fitContainerHeight();
        }
    }


//======================================================================================================================
// SETTERS
//======================================================================================================================

    private void setStyle() {
        container.getStylesheets().add(loader.getResource(STYLESHEET_PATH).toExternalForm());

        container.getStyleClass().add(STYLE_BACKGROUND);
        scrollpane.getStyleClass().add(STYLE_SCROLLER);
        wifiSearchLabel.getStyleClass().add(STYLE_WIFI_SEARCH_LABEL);
    }


//======================================================================================================================
// NETWORKS FRAGMENT MANAGER
//======================================================================================================================

    private void addNetworkFC(WifiNetwork wifiNetwork) {
        WifiNetworkFragmentController wnFC = new WifiNetworkFragmentController(this, wifiNetwork);
        vbox.getChildren().add(wnFC);
        wifiNetworkFCVector.add(wnFC);

        fitContainerHeight();
    }

    private void removeNetworkFC(WifiNetworkFragmentController wifiNetwork) {
        wifiNetworkFCVector.remove(wifiNetwork);
        vbox.getChildren().remove(wifiNetwork);

        fitContainerHeight();
    }

    public void fitContainerHeight() {
        int numVisible = wifiNetworkFCVector.size();

        if(lastWifiFragmentWithInfo != null) {
            if(lastWifiFragmentWithInfo.isWifiInfoShow()) {
                numVisible++;
            }
        }

        if (numVisible > SCROLLPANE_NUM_VISIBLE) {
            vbox.setPrefWidth(HORIZONTAL_RESOLUTION_MINIMAL);
        } else {
            vbox.setPrefWidth(HORIZONTAL_RESOLUTION_MAXIMUM);
        }
    }

    private void checkCurrentWifiNetwork(String currentSSID) {
        if(!lastCurrentWifiNetwork.equals(currentSSID)) {
            lastCurrentWifiNetwork = currentSSID;

            for(WifiNetworkFragmentController wifiNetworkFC : wifiNetworkFCVector){
                if(wifiNetworkFC.getWifiNetwork().getSsid().equals(currentSSID)) {
                    wifiNetworkFC.markCurrentWifiNetwork();
                } else {
                    wifiNetworkFC.uncheckCurrentWifiNetwork();
                }
            }
        }
    }

    private void updateWifiNetworks(Vector<WifiNetwork> wifiNetworkVector) {
        // update stagnation
        Vector<WifiNetworkFragmentController> wifiNetworksToDelete = new Vector<WifiNetworkFragmentController>();
        for(WifiNetworkFragmentController wifiNetworkFC : wifiNetworkFCVector) {
            if(wifiNetworkFC.getStagnation() > WIFi_MAX_STAGNATION_NUM) {
                wifiNetworksToDelete.add(wifiNetworkFC);
            } else {
                wifiNetworkFC.setStagnation(wifiNetworkFC.getStagnation() + 1);
            }
        }
        // delete disappeared wifi networks
        for(WifiNetworkFragmentController wifiNetworkFC : wifiNetworksToDelete) {
            removeNetworkFC(wifiNetworkFC);
        }

        // add new
        for(WifiNetwork wifiNetwork : wifiNetworkVector) {
            boolean isFound = false;
            for(WifiNetworkFragmentController wifiNetworkFC : wifiNetworkFCVector){
                if(wifiNetwork.getSsid().equals(wifiNetworkFC.getWifiNetwork().getSsid())) {
                    wifiNetworkFC.setStagnation(0);
                    isFound = true;
                    break;
                }
            }

            if(!isFound)
                addNetworkFC(wifiNetwork);
        }
    }



}
