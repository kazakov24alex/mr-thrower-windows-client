package com.mr_thrower.application.scenes;

import com.mr_thrower.application.SceneSelector;
import com.mr_thrower.application.database.DbManager;
import com.mr_thrower.application.service.AppService;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import library.codes.ThrowModeCodes;
import org.apache.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;


public class WelcomeSceneController implements Initializable {
    private static Logger log = Logger.getLogger(WelcomeSceneController.class.getName());
    private ClassLoader loader;


//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    public final static String PATH_IMAGE_BT_DARK = "images/bt_dark_128.png";
    public final static String PATH_IMAGE_BT_WHITE = "images/bt_white_128.png";
    public final static String IMAGE_PATH_WIFI_DARK = "images/wifi_dark_128.png";
    public final static String PATH_IMAGE_DIRECT_DARK = "images/direct_dark_128.png";


    private static final String STYLE_LABEL_MODE = "-fx-text-fill: white";
    private static final String STYLE_LABEL_MODE_MOVED = "-fx-text-fill: #38003D";


//======================================================================================================================
// FXML NODES
//======================================================================================================================

    @FXML private AnchorPane mainPane;

    @FXML private ImageView bluetoothImageView;
    @FXML private ImageView wifiImageView;
    @FXML private ImageView directImageView;

    @FXML private Label bluetoothLabel;
    @FXML private Label wifiNetworkLabel;
    @FXML private Label wifiDirectLabel;


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private Image bluetoothWhiteImage;
    private Image wifiNetDarkImage;
    private Image wifiDirectDarkImage;

    private Image bluetoothOriginalImage;
    private Image wifiOriginalImage;
    private Image directOriginalImage;

    private boolean isBluetoothHover;
    private boolean isWifiNetHover;
    private boolean isWifiDirectHover;


//======================================================================================================================
// INITIALIZATION
//======================================================================================================================

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loader = WelcomeSceneController.class.getClassLoader();

        AppService.getInstance().stop();

        imageInitialization();

        isBluetoothHover = false;
        isWifiNetHover = false;
        isWifiDirectHover = false;

        // Database connecting
        DbManager.getInstance().initAccessToDB();
    }


//======================================================================================================================
// SETTERS
//======================================================================================================================

    private void imageInitialization() {
        bluetoothWhiteImage = new Image(loader.getResource(PATH_IMAGE_BT_WHITE).toString());
        bluetoothOriginalImage = new Image(loader.getResource(PATH_IMAGE_BT_DARK).toString());
        wifiOriginalImage = new Image(loader.getResource(IMAGE_PATH_WIFI_DARK).toString());
        directOriginalImage = new Image(loader.getResource(PATH_IMAGE_DIRECT_DARK).toString());

        bluetoothImageView.setImage(bluetoothOriginalImage);
        wifiImageView.setImage(wifiOriginalImage);
        directImageView.setImage(directOriginalImage);


        setStyles();
    }

    private void setStyles() {
        bluetoothLabel.setStyle(STYLE_LABEL_MODE);
        wifiNetworkLabel.setStyle(STYLE_LABEL_MODE);
        wifiDirectLabel.setStyle(STYLE_LABEL_MODE);
    }

//======================================================================================================================
// BLUETOOTH PART CALLBACKS
//======================================================================================================================

    @FXML
    void onBluetoothMouseClicked() {
        log.info("BLUETOOTH part was clicked");
        AppService.getInstance().setMode(ThrowModeCodes.BLUETOOTH);
        SceneSelector.getInstance().setMainScene();
    }

    @FXML
    void onBluetoothMouseMoved() {
        if(!isBluetoothHover) {
            //log.info("BLUETOOTH part was moved");
            bluetoothImageView.setImage(bluetoothWhiteImage);
            //bluetoothLabel.setStyle(STYLE_LABEL_MODE_MOVED);
            isBluetoothHover = true;
        }
    }

    @FXML
    void onBluetoothMouseExited() {
        bluetoothImageView.setImage(bluetoothOriginalImage);
        bluetoothLabel.setStyle(STYLE_LABEL_MODE);
        isBluetoothHover = false;
    }


//======================================================================================================================
// WIFI NETWORK PART CALLBACKS
//======================================================================================================================

    @FXML
    void onWifiNetworkMouseClicked () {
        log.info("WIFI NETWORK part was clicked");
        AppService.getInstance().setMode(ThrowModeCodes.WIFI_NETWORK);
        SceneSelector.getInstance().setMainScene();
    }

    @FXML
    void onWifiNetworkMouseMoved() {
        if(!isWifiNetHover) {
            //log.info("WIFI NETWORK part was moved");
            //wifiImageView.setImage(wifiNetDarkImage);
            wifiNetworkLabel.setStyle(STYLE_LABEL_MODE_MOVED);
            isWifiNetHover = true;
        }
    }

    @FXML
    void onWifiNetworkMouseExited() {
        wifiImageView.setImage(wifiOriginalImage);
        wifiNetworkLabel.setStyle(STYLE_LABEL_MODE);
        isWifiNetHover = false;
    }


//======================================================================================================================
// WIFI DIRECT PART CALLBACKS
//======================================================================================================================

    @FXML
    void onWifiDirectMouseClicked() {
        log.info("WIFI DIRECT part was clicked");
        AppService.getInstance().setMode(ThrowModeCodes.WIFI_DIRECT);
        SceneSelector.getInstance().setMainScene();
    }

    @FXML
    void onWifiDirectMouseMoved() {
        if(!isWifiDirectHover) {
            //log.info("WIFI DIRECT part was moved");
            //directImageView.setImage(wifiDirectDarkImage);
            wifiDirectLabel.setStyle(STYLE_LABEL_MODE_MOVED);
            isWifiDirectHover = true;
        }
    }

    @FXML
    void onWifiDirectMouseExited() {
        directImageView.setImage(directOriginalImage);
        wifiDirectLabel.setStyle(STYLE_LABEL_MODE);
        isWifiDirectHover = false;
    }

}
