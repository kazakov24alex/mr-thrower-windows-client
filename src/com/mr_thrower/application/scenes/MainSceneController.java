package com.mr_thrower.application.scenes;

import com.mr_thrower.application.database.DbManager;
import com.mr_thrower.application.fragments.chat_side.ChatFragmentController;
import com.mr_thrower.application.fragments.chat_side.InputLineFragmentController;
import com.mr_thrower.application.fragments.chat_side.TitleBarFragmentController;
import com.mr_thrower.application.fragments.contacts_side.ContactFragmentController;
import com.mr_thrower.application.fragments.contacts_side.MainInfoFragmentController;
import com.mr_thrower.application.monitors.NetExpert;
import com.mr_thrower.application.monitors.interfaces.IWifiDataMonitorListener;
import com.mr_thrower.application.service.AppService;
import library.wrappers.MessageWrapper;
import library.wrappers.ThrowWrapper;
import com.mr_thrower.application.wrappers.WifiData;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import library.codes.DirectionCodes;
import library.codes.PacketCodes;
import library.codes.ThrowModeCodes;
import library.codes.ThrowTypeCodes;
import library.interfaces.event_manager.IEventListener;
import library.objects.*;
import library.wrappers.EventWrapper;
import library.wrappers.MacAddress;
import org.apache.log4j.Logger;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.Vector;

public class MainSceneController implements Initializable, IWifiDataMonitorListener, IEventListener {
    private static Logger log = Logger.getLogger(MainSceneController.class.getName());
    private ClassLoader loader;

//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private final static String PATH_IMAGE_WIFI_LOGO = "images/wifi_gray_512.png";
    private final static String PATH_IMAGE_BT_LOGO = "images/bt_gray_512.png";
    private final static String PATH_IMAGE_DIRECT_LOGO = "images/direct_gray_512.png";


    private static final String STYLESHEET_PATH = "styles/main_scene.css";
    private static final String STYLE_BACKGROUND = "background";
    private static final String STYLE_CHAT_CONTAINER = "chat-container";

    private final static int CONTACTS_NUM_VISIBLE = 4;


//======================================================================================================================
// FXML NODES
//======================================================================================================================

    @FXML private GridPane gridPaneNeed;

    @FXML private VBox mainInfoVBox;
    @FXML private ScrollPane contactsScrollPane;
    @FXML private VBox contactVbox;

    @FXML private AnchorPane titleBarContainer;
    @FXML private AnchorPane chatContainer;
    @FXML private AnchorPane inputLineContainer;

    @FXML private ImageView modeImage;



//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private Byte mode;

    // LEFT SIDE
    private MainInfoFragmentController infoFC;
    private Vector<ContactFragmentController> contactsVector;

    // RIGHT SIDE
    private TitleBarFragmentController titleBarFC;
    private ChatFragmentController chatFC;
    private InputLineFragmentController inputLineFC;

    // current data
    private String lastSSID;
    private ContactFragmentController openedContact;


//======================================================================================================================
// INITIALIZATION
//======================================================================================================================

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loader = getClass().getClassLoader();
        mode = AppService.getInstance().getMode();

        contactsVector = new Vector<>();
        lastSSID = " ";
        openedContact = null;

        switch (mode) {
            case ThrowModeCodes.WIFI_NETWORK:
                modeImage.setImage(new Image(loader.getResource(PATH_IMAGE_WIFI_LOGO).toString()));
                break;
            case ThrowModeCodes.BLUETOOTH:
                modeImage.setImage(new Image(loader.getResource(PATH_IMAGE_BT_LOGO).toString()));
                break;
            case ThrowModeCodes.WIFI_DIRECT:
                modeImage.setImage(new Image(loader.getResource(PATH_IMAGE_DIRECT_LOGO).toString()));
                break;
        }


        // LEFT SIDE INITIALIZATION
        infoFC = new MainInfoFragmentController(this, mode);
        infoFC.setName(DbManager.getInstance().getProfile().getName());
        infoFC.setDeviceName(DbManager.getInstance().getProfile().getDevice());
        mainInfoVBox.getChildren().add(infoFC);


        // RIGHT SIDE INITIALIZATION
        titleBarFC = new TitleBarFragmentController(this);
        titleBarContainer.getChildren().add(titleBarFC);
        titleBarFC.setVisible(false);

        chatFC = new ChatFragmentController();
        chatContainer.getChildren().add(chatFC);
        chatFC.setVisible(false);

        inputLineFC = new InputLineFragmentController(mode);
        inputLineContainer.getChildren().add(inputLineFC);
        inputLineFC.setVisible(false);


        // SERVICE INITIALIZATION
        EventBus.getDefault().register(this);
        lastSSID = NetExpert.getCurrentSSID();
        infoFC.setWifiNetName(lastSSID);
        AppService.getInstance().start();

        // tmp
        if(mode == ThrowModeCodes.BLUETOOTH)
            infoFC.setWifiNetName("BLUETOOTH");

        if(mode == ThrowModeCodes.WIFI_DIRECT)
            EventBus.getDefault().post(new EventWrapper(new ContactsRequest(), EventWrapper.TO_EVENT_MANAGER));
    }


    private void setStyles() {
        contactsScrollPane.getStylesheets().add(loader.getResource(STYLESHEET_PATH).toExternalForm());

        contactsScrollPane.getStyleClass().add(STYLE_BACKGROUND);
        chatContainer.getStyleClass().add(STYLE_CHAT_CONTAINER);
    }


//=================================================================n=====================================================
// IWifiDataMonitorListener
//======================================================================================================================

    @Subscribe(threadMode = ThreadMode.MAIN)
    @Override
    public void onWifiDataUpdate(WifiData wifiData) {
        Platform.runLater(() -> {
            // start research if network is changed
            if (!lastSSID.equals(wifiData.getCurrentSSID())) {
                lastSSID = wifiData.getCurrentSSID();
                clearContactsList();

                if (wifiData.getCurrentSSID().equals(NetExpert.STATE_WIFI_NOT_CONNECTED)) {
                    infoFC.setWifiNetName("NOT CONNECTED"); // TODO locale
                } else {
                    infoFC.setWifiNetName(wifiData.getCurrentSSID());
                    log.info("NEW SSID = " + wifiData.getCurrentSSID());

                    AppService.getInstance().restart(ThrowModeCodes.WIFI_NETWORK);
                }
            }
        });
    }


//======================================================================================================================
// EVENT LISTENER AND DISTRIBUTOR
//======================================================================================================================

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(EventWrapper event) {
        Platform.runLater(() -> {
            // check event direction
            if (event.getDirection() != EventWrapper.FROM_EVENT_MANAGER)
                return;

            // identify the type of event and select the callback
            switch (event.getCode()) {
                case PacketCodes.GREETING:
                    onGreetingEvent((GreetingObject) event.getObject());
                    break;
                case PacketCodes.GOODBYE:
                    onGoodbyeEvent((GoodbyeObject) event.getObject());
                    break;
                case PacketCodes.MESSAGE:
                    onMessageEvent((MessageObject) event.getObject());
                    break;
                case PacketCodes.MESSAGE_ACK:
                    onMessageAckEvent((MessageAckObject) event.getObject());
                    break;
                case PacketCodes.THROW_REQUEST:
                    onThrowRequestEvent((ThrowRequestObject) event.getObject());
                    break;
                case PacketCodes.THROW_RESPONSE:
                    onThrowResponseEvent((ThrowResponseObject) event.getObject());
                    break;
                case PacketCodes.THROW_PROGRESS:
                    onThrowProgressEvent((ThrowProgressObject) event.getObject());
                    break;
                case PacketCodes.THROW_END_ACK:
                    onThrowEndAckEvent((ThrowEndAckObject) event.getObject());
                    break;
            }
        });
    }


    @Override
    public void onGreetingEvent(GreetingObject greeting) {
        for(ContactFragmentController contactFC : contactsVector) {
            if(contactFC.getMacAddress().compareTo(greeting.getMacAddress()) == 0)
                return;
        }

        ContactFragmentController cfc = new ContactFragmentController(greeting,mode,this);
        addContact(cfc);
    }

    @Override
    public void onGoodbyeEvent(GoodbyeObject goodbye) {
        log.info("DEVICE GOODBYE    : "+goodbye.getMac());
        for(ContactFragmentController cfc : contactsVector) {
            if(cfc.getMacAddress().compareTo(goodbye.getMac()) == 0) {
                deleteContact(cfc);
                return;
            }
        }
    }

    @Override
    public void onMessageEvent(MessageObject message) {
        // TODO
        if(openedContact != null) {
            if (openedContact.getMacAddress().compareTo(message.getAddress()) == 0) {
                // TODO
                MessageWrapper mesWr = new MessageWrapper(message, DirectionCodes.INCOMING, mode);
                chatFC.pushMessage(mesWr);
            }
        } else {
            // increment notification label
            for (ContactFragmentController cfc : contactsVector) {
                if (message.getAddress().compareTo(cfc.getMacAddress()) == 0) {
                    cfc.incrementCounter();
                }
            }
        }
    }

    @Override
    public void onMessageAckEvent(MessageAckObject messageAckObject) {
        // TODO
        log.info("MESSAGE ACK RECEIVED");
    }

    @Override
    public void onThrowRequestEvent(ThrowRequestObject request) {
        if(openedContact != null) {
            if (openedContact.getMacAddress().compareTo(request.getAddress()) == 0) {
                for(ThrowFileObject tFile : request.getThrowFiles()) {
                    ThrowWrapper throwWr = new ThrowWrapper(tFile, DirectionCodes.INCOMING, mode, ThrowTypeCodes.FILE);
                    chatFC.pushThrow(throwWr);
                }
            }
        } else {
            // increment notification label
            for (ContactFragmentController cfc : contactsVector) {
                if (request.getAddress().compareTo(cfc.getMacAddress()) == 0) {
                    for(ThrowFileObject tFile : request.getThrowFiles()) {
                        cfc.incrementCounter();
                    }
                }
            }
        }

        // TODO dialogAlert
        EventBus.getDefault().post(new EventWrapper(request, EventWrapper.TO_THROW_MANAGER));
    }

    @Override
    public void onThrowResponseEvent(ThrowResponseObject response) {
        // TODO
        log.info("RESPONSE RECEIVED");
    }

    @Override
    public void onThrowProgressEvent(ThrowProgressObject progress) {
        if(openedContact != null) {
            if (openedContact.getMacAddress().compareTo(progress.getAddress()) == 0) {
                chatFC.pushProgress(progress);
            }
        }

        log.info("PROGRESS RECEIVED");
    }

    @Override
    public void onThrowEndAckEvent(ThrowEndAckObject endAck) {
        if(openedContact != null) {
            if (openedContact.getMacAddress().compareTo(endAck.getAddress()) == 0) {
                chatFC.pushEndAck(endAck);
            }
        }

        log.info("END ACK RECEIVED");
    }

//======================================================================================================================
// CONTACTS LIST
//======================================================================================================================

    private void addContact(ContactFragmentController contactFC) {
        contactVbox.getChildren().add(contactFC);
        contactsVector.add(contactFC);

        if(contactsVector.size() > CONTACTS_NUM_VISIBLE) {
            contactVbox.setPrefWidth(ContactFragmentController.CONTACTS_WIDTH_SHORT);
        }
    }

    private void deleteContact(ContactFragmentController contactFC) {
        contactVbox.getChildren().remove(contactFC);
        contactsVector.remove(contactFC);

        if(contactsVector.size() <= CONTACTS_NUM_VISIBLE) {
            contactVbox.setPrefWidth(ContactFragmentController.CONTACTS_WIDTH_LONG);
        }
    }

    private void clearContactsList() {
        Vector<ContactFragmentController> vector = new Vector<>(contactsVector);
        for(ContactFragmentController cfc : vector) {
            deleteContact(cfc);
        }
    }




//======================================================================================================================
// CALLBACKS
//======================================================================================================================

    public void onChatClosed() {
        openedContact.setUnselected();
        openedContact = null;

        titleBarFC.setVisible(false);
        chatFC.setVisible(false);
        inputLineFC.setVisible(false);

        modeImage.setVisible(true);
    }

    public void onContactSelected(ContactFragmentController contactFC) {
        if(openedContact != null)
            openedContact.setUnselected();

        // hide mode logo
        modeImage.setVisible(false);
        // show title bar
        titleBarFC.setTitle(contactFC.getContactName());
        titleBarFC.setVisible(true);
        // show chat pane
        chatFC.changeContact(contactFC.getMacAddress());
        chatFC.setVisible(true);
        // show input line
        inputLineFC.changeContact(contactFC.getMacAddress(), chatFC);
        inputLineFC.setVisible(true);

        openedContact = contactFC;
        openedContact.setSelected();
        openedContact.setCounterToZero();
    }

}
