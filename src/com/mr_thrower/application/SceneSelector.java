package com.mr_thrower.application;


import com.mr_thrower.application.service.AppService;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import org.apache.log4j.Logger;


import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

public class SceneSelector {
    private static Logger log = Logger.getLogger(SceneSelector.class.getName());


//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    public final static int RESOLUTION_HORIZONTAL = 800;
    public final static int RESOLUTION_VERTICAL = 450;

    public final static String FXML_PATH_WELCOME_SCENE = "scenes/welcome_scene.fxml";
    public final static String FXML_PATH_WIFI_NETWORK_SCENE = "scenes/main_scene.fxml";
    public final static String FXML_PATH_WIFI_SELECT_SCENE = "scenes/wifi_select_scene.fxml";

    public final static String RESOURCES_LOCALE = "com.mr_thrower.resources.bundles.locale";
    public final static String LANGUAGE_DEFAULT = "en";


//======================================================================================================================
// SINGLETON
//======================================================================================================================

    private static SceneSelector instance;

    public static synchronized SceneSelector getInstance() {
        if (instance == null) {
            instance = new SceneSelector();
        }
        return instance;
    }


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private ClassLoader classLoader;

    private Stage primaryStage;
    private Stage secondScene;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    SceneSelector() {
        primaryStage = null;
        secondScene = null;

        classLoader = SceneSelector.class.getClassLoader();
    }

    public void setPrimaryStage(Stage primaryStage) {
        this.primaryStage = primaryStage;

        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            @Override
            public void handle(WindowEvent t) {
                log.info("APPLICATION CLOSED");
                AppService.getInstance().stop();

                Platform.exit();
                System.exit(0);
            }
        });
    }


//======================================================================================================================
// SCENE SETTERS
//======================================================================================================================

    public void setWelcomeScene() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(classLoader.getResource(FXML_PATH_WELCOME_SCENE));
            loader.setResources(ResourceBundle.getBundle(RESOURCES_LOCALE, new Locale(LANGUAGE_DEFAULT)));
            Parent root = loader.load();

            Scene scene = new Scene(root, RESOLUTION_HORIZONTAL, RESOLUTION_VERTICAL);
            primaryStage.setScene(scene);
            primaryStage.setTitle(loader.getResources().getString("application.name"));
            primaryStage.setResizable(false);
            primaryStage.show();

            //WelcomeSceneController welcomeSC = loader.getController();
        } catch (IOException e) {
            log.error("Set WelcomeStage exception:");
            e.printStackTrace();
        }
    }

    public void setMainScene() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(classLoader.getResource(FXML_PATH_WIFI_NETWORK_SCENE));
            loader.setResources(ResourceBundle.getBundle(RESOURCES_LOCALE, new Locale(LANGUAGE_DEFAULT)));
            Parent root = loader.load();

            Scene scene = new Scene(root, RESOLUTION_HORIZONTAL, RESOLUTION_VERTICAL);
            primaryStage.setScene(scene);
            primaryStage.setTitle(loader.getResources().getString("application.name"));
            primaryStage.setResizable(false);
            primaryStage.show();

        } catch (IOException e) {
            log.error("Set WifiNetworkStage exception:");
            e.printStackTrace();
        }
    }

    public void showWifiChoiceWindow() {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(classLoader.getResource(FXML_PATH_WIFI_SELECT_SCENE));
            loader.setResources(ResourceBundle.getBundle(RESOURCES_LOCALE, new Locale(LANGUAGE_DEFAULT)));
            Parent root = loader.load();

            Scene secondScene = new Scene(root, 270, 366);

            //New window (Stage)
            Stage newWindow = new Stage();
            newWindow.setTitle("Select WiFi");
            newWindow.setScene(secondScene);
            newWindow.setResizable(false);

            // Specifies the modality for new window.
            newWindow.initModality(Modality.WINDOW_MODAL);

            // Specifies the owner Window (parent) for new window
            newWindow.initOwner(primaryStage);

            // Set position of second window, related to primary window.
            newWindow.setX(primaryStage.getX() + 262);
            newWindow.setY(primaryStage.getY() - 40);

            this.secondScene = newWindow;
            newWindow.show();

        } catch (IOException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }

    }

    public void closeSecondScene() {
        secondScene.close();
    }


    public List<File> showFileChooser() {
        FileChooser fileChooser = new FileChooser();
        return fileChooser.showOpenMultipleDialog(primaryStage);
    }
}
