package com.mr_thrower.application.service.wifi_module;

import library.interfaces.throw_manager.IThrowManagerInformer;
import library.interfaces.throw_threads.ICatchThread;
import library.objects.ThrowEndAckObject;
import library.objects.ThrowFileObject;
import library.objects.ThrowProgressObject;
import library.objects.ThrowResponseObject;
import library.wrappers.EventWrapper;
import org.apache.log4j.Logger;
import org.greenrobot.eventbus.EventBus;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class WifiCatchThread extends Thread implements ICatchThread {
    private static Logger log = Logger.getLogger(WifiThrowThread.class.getName());

//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private final int CHUNK_SIZE = 32768;
    private final int PROGRESS_UPDATE_LIMIT = 100 * 1024;  // bytes


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private IThrowManagerInformer throwManager;
    private ThrowFileObject catchFile;

    private boolean isCompleted;

    private ServerSocket serverSocket;
    private Socket incomingSocket;
    private DataInputStream dataInputStream;
    private FileOutputStream fileOutputStream;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public WifiCatchThread(IThrowManagerInformer throwManager, ThrowFileObject catchFile) {
        this.throwManager = throwManager;
        this.catchFile = catchFile;

        isCompleted = false;

        // daemon-thread setting
        setDaemon(true);
        // determining the default priority for the thread
        setPriority(NORM_PRIORITY);
    }


//======================================================================================================================
// ICatchThread
//======================================================================================================================

    @Override
    public void activate() {
        super.start();
    }

    @Override
    public void deactivate() {
        super.interrupt();
    }


//======================================================================================================================
// OPERATING CYCLE
//======================================================================================================================

    @Override
    public void run() {
        connectToThrower();
        catchFile();
    }

    @Override
    public void interrupt() {
        try {
            if(fileOutputStream != null)
                fileOutputStream.close();
            if(dataInputStream != null)
                dataInputStream.close();
            if(incomingSocket != null)
                incomingSocket.close();
            if(serverSocket != null)
                serverSocket.close();

            if(!isCompleted) {
                // TODO delete file
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        super.interrupt();
    }


//======================================================================================================================
// OPERATING METHODS
//======================================================================================================================

    private void connectToThrower() {
        // create server socket on available port
        try {
            serverSocket = new ServerSocket(0);

            // send ThrowResponse with file TimeID and CONNECTION_PORT
            ThrowResponseObject response = new ThrowResponseObject(catchFile.getTimeID(), serverSocket.getLocalPort());
            response.setAddress(catchFile.getAddress());
            EventBus.getDefault().post(new EventWrapper(response, EventWrapper.TO_EVENT_MANAGER));

            // connection waiting
            log.info("Waiting for thrower to connect ...");
            incomingSocket = serverSocket.accept();
            log.info("... thrower connected on Port=" + incomingSocket.getLocalPort());

        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    private void catchFile() {
        log.info("WIFI-Catch process started");

        try {
            dataInputStream = new DataInputStream(incomingSocket.getInputStream());

            // limit of progress dispatch
            long progressLimit = PROGRESS_UPDATE_LIMIT;

            long bytesReceived = 0;

            fileOutputStream = new FileOutputStream(this.catchFile.getFilename());

            while (true){
                // read in pieces
                byte[] chunk = new byte[CHUNK_SIZE];
                int readBytesCount = dataInputStream.read(chunk);

                if (readBytesCount > 0) {
                    fileOutputStream.write(chunk, 0, readBytesCount);

                    bytesReceived += readBytesCount;

                    // send progress
                    if(bytesReceived > progressLimit) {
                        ThrowProgressObject progress = new ThrowProgressObject(this.catchFile.getTimeID(), bytesReceived);
                        progress.setAddress(catchFile.getAddress());
                        EventBus.getDefault().post(new EventWrapper(progress, EventWrapper.TO_EVENT_MANAGER));

                        progressLimit += PROGRESS_UPDATE_LIMIT;
                    }

                    if (bytesReceived == catchFile.getFileSize())
                        break;
                }
            }

            // close file
            fileOutputStream.flush();
            fileOutputStream.close();
            fileOutputStream = null;

            isCompleted = (bytesReceived == catchFile.getFileSize());

            // send end ack notification
            final String dir = System.getProperty("user.dir");
            ThrowEndAckObject endAck = new ThrowEndAckObject(this.catchFile.getTimeID(), isCompleted);
            endAck.setAddress(catchFile.getAddress());
            throwManager.transmitThrowEndAck(endAck, dir + "\\" + catchFile.getFilename()); // TODO path

        } catch (IOException e) {
            e.printStackTrace();
        }

        log.info("WIFI-Catch process finished");
    }

}