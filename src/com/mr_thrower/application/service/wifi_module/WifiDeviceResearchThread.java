package com.mr_thrower.application.service.wifi_module;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;


public class WifiDeviceResearchThread extends Thread {
    private static Logger log = Logger.getLogger(WifiDeviceResearchThread.class.getName());


//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private static int TIMEOUT = 1000;
    private static int CONNECTION_PORT = WifiConnectionManager.CONNECTION_PORT;


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private String host;
    private WifiConnectionManager wifiConnectionManager;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public WifiDeviceResearchThread(String host, WifiConnectionManager wifiConnectionManager) {
        this.host = host;
        this.wifiConnectionManager = wifiConnectionManager;

        // daemon-thread setting
        setDaemon(true);
        // determining the default priority for the thread
        setPriority(NORM_PRIORITY);
    }


//======================================================================================================================
// OPERATING CYCLE
//======================================================================================================================

    @Override
    public void run() {
        super.run();

        try {
            // ping device this this IP address
            if (InetAddress.getByName(host).isReachable(TIMEOUT)){
                log.info(host + " is reachable");

                InetAddress ipAddress = InetAddress.getByName(host);
                Socket socket = new Socket(ipAddress, CONNECTION_PORT);
                log.info(host + " device use this program");

                wifiConnectionManager.onDeviceFound(socket);
            }

        } catch(IOException e){
            log.info(host + " device does not use this program");
        }

        interrupt();
    }

    @Override
    public void interrupt() {
        super.interrupt();
    }


}
