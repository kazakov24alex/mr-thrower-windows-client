package com.mr_thrower.application.service.wifi_module;

import library.interfaces.throw_manager.IThrowManagerInformer;
import library.interfaces.throw_threads.IThrowThread;
import library.objects.ThrowFileObject;
import library.wrappers.IpAddress;
import library.wrappers.MacAddress;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.Socket;
import java.util.Date;

public class WifiThrowThread extends Thread implements IThrowThread {
    private static Logger log = Logger.getLogger(WifiThrowThread.class.getName());


//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private final static int CHUNK_SIZE = 32768;


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private IThrowManagerInformer throwManager;
    private ThrowFileObject throwFile;

    private MacAddress macAddress;
    private int port;

    private boolean isRunning;

    private Socket socket;
    private OutputStream outputStream;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public WifiThrowThread(IThrowManagerInformer throwManager, ThrowFileObject throwFile, MacAddress macAddress, int port) {
        this.throwManager = throwManager;
        this.throwFile = throwFile;

        this.macAddress = macAddress;
        this.port = port;

        isRunning = false;

        // daemon-thread setting
        setDaemon(true);

        // determining the default priority for the thread
        setPriority(NORM_PRIORITY);


    }


//======================================================================================================================
// ICatchThread
//======================================================================================================================

    @Override
    public void activate() {
        super.start();
    }

    @Override
    public void deactivate() {
        super.interrupt();
    }

    @Override
    public Date getTimeID() {
        return throwFile.getTimeID();
    }


//======================================================================================================================
// OPERATING CYCLE
//======================================================================================================================

    @Override
    public void run() {
        isRunning = true;

        connectToCatcher();
        throwFile();
    }

    @Override
    public void interrupt() {
        isRunning = false;

        try {
            if (outputStream != null)
                outputStream.close();

            if (socket != null)
                socket.close();

        } catch (IOException e) {
            log.error("WifiThrowThread interrupting exception: " + e.getMessage());
            e.printStackTrace();
        }

        log.info("thread stopped");
        super.interrupt();
    }



//======================================================================================================================
// OPERATING METHODS
//======================================================================================================================

    private void connectToCatcher() {
        log.info("Connecting to catching device...");

        // get host
        IpAddress ipAddress = throwManager.getIpAddressByMac(throwFile.getAddress());

        if(ipAddress == null) {
            log.info("catching device not found");
            throwManager.noContact(macAddress);
        }

        try {
            // set connection
            socket = new Socket(ipAddress.toString(), port);
        } catch(IOException e) {
            e.printStackTrace();
        }

        log.info("... catching device was connected on Port=" + port);
    }

    private void throwFile() {
        log.info("WIFI-Throw process started");

        try {
            outputStream = socket.getOutputStream();

            FileInputStream fis = new FileInputStream(throwFile.getPath());

            // send file in pieces (CHUNKS)
            long bytesLeft = throwFile.getFileSize();
            byte[] chunkBuffer = new byte[CHUNK_SIZE];

            while(bytesLeft > CHUNK_SIZE) {
                fis.read(chunkBuffer);
                outputStream.write(chunkBuffer, 0, CHUNK_SIZE);

                bytesLeft -= CHUNK_SIZE;
            }

            // send file reminder
            byte[] remainderBuffer = new byte[(int) bytesLeft];
            fis.read(remainderBuffer);
            outputStream.write(remainderBuffer, 0, remainderBuffer.length);
            outputStream.flush();

            // close file stream
            fis.close();

            log.info("WIFI-Throw process finished");

        } catch(IOException e) {
            log.error("Sending file exception: " + e.getMessage());
            e.printStackTrace();
        }

    }
}
