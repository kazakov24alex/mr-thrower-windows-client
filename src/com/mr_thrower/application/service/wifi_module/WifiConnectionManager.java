package com.mr_thrower.application.service.wifi_module;

import library.BytePacket;
import library.interfaces.IConnectionManager;
import library.interfaces.event_manager.IEventReceiver;
import library.objects.GoodbyeObject;
import library.wrappers.IpAddress;
import library.wrappers.MacAddress;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


public class WifiConnectionManager extends Thread implements IConnectionManager {
    private static Logger log = Logger.getLogger(WifiConnectionManager.class.getName());

    private int POOL_SIZE = 20;
    public static int CONNECTION_PORT = 2424;


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private IEventReceiver eventManager;
    private Map<String, WifiProtocolThread>  mConnectionPool;     // String = MacAddress.toString()

    private boolean isRunning;

    private ServerSocket serverSocket;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================
    public WifiConnectionManager(IEventReceiver eventManager) {
        this.eventManager = eventManager;
        mConnectionPool = new HashMap<>();

        isRunning = false;
    }


//======================================================================================================================
// IConnectionManager
//======================================================================================================================
    @Override
    public void activate() {
        start();
    }

    @Override
    public void deactivate() {
        interrupt();
    }

    @Override
    public void specifyMacToConnection(MacAddress macAddress) {
        WifiProtocolThread connection = mConnectionPool.get(null);

        if(connection != null) {
            connection.setMacAddress(macAddress);

            mConnectionPool.put(macAddress.toString(), connection);
            mConnectionPool.remove(null);

            log.info("mac address " + macAddress.toString() + " was specified for connection");
        }
    }

    @Override
    public void sendPacket(MacAddress macAddress, BytePacket packet) {
        mConnectionPool.get(macAddress.toString()).writeSocket(packet);
    }


//======================================================================================================================
// OPERATING CYCLE // Thread
//======================================================================================================================
    @Override
    public void run() {
        super.run();
        isRunning = true;

        log.info("WIFI connection manager started");

        try {
            // creating server socket on CONNECTION_PORT
            serverSocket = new ServerSocket(CONNECTION_PORT, 1);
            log.info("ServerSocket CONNECTION_PORT: " + CONNECTION_PORT);

            while (isRunning) {
                log.info("Waiting for clients to connect...");

                // waiting client connection
                Socket incomingSocket = serverSocket.accept();

                // connection occurred
                log.info("... client accepted");

                // create a thread to process the connection
                WifiProtocolThread connection = new WifiProtocolThread(this, incomingSocket, false);
                addConnection(new MacAddress(), connection);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void interrupt() {
        // stop server socket
        try {
            if (serverSocket != null) {
                serverSocket.close();
                serverSocket = null;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // stop connection listener threads
        for (Map.Entry<String, WifiProtocolThread> pair : mConnectionPool.entrySet()) {
            pair.getValue().interrupt();
        }
        mConnectionPool.clear();

        isRunning = false;

        log.info("WIFI connection manager stopped");
        super.interrupt();
    }


//======================================================================================================================
// CALLBACKS
//======================================================================================================================

    public void packetReceived(MacAddress macAddress, BytePacket bytePacket) {
        eventManager.onReceivedPacket(macAddress, bytePacket);
    }

    public void onDeviceFound(Socket socket) {
        // create a thread to process the connection
        WifiProtocolThread connection = new WifiProtocolThread(this, socket, true);
        addConnection(new MacAddress(), connection);
    }

    private void addConnection(MacAddress macAddress, WifiProtocolThread connection) {
        if(mConnectionPool.size() < POOL_SIZE) {
            mConnectionPool.put(macAddress.toString(), connection);

            log.info("connection was added to Connection Pool");
            connection.start();

        } else {
            log.info("connection was rejected (Connection Pool is overflowed");
            connection.interrupt();
        }
    }

    public void deleteConnection(WifiProtocolThread connectionThread) {
        mConnectionPool.remove(connectionThread);

        // emulate Goodbye packet receiving
        GoodbyeObject goodbye = new GoodbyeObject(connectionThread.getMacAddress());
        eventManager.onReceivedPacket(connectionThread.getMacAddress(), goodbye.getBytePacket());

        log.info("device ["+connectionThread.getMacAddress().toString()+"] was disconnected");
    }


//======================================================================================================================
// AUXILIARY METHODS
//======================================================================================================================

    @Override
    public ArrayList<MacAddress> getActualMacAddresses() {
        ArrayList<MacAddress> macs = new ArrayList<>();

        Set<Map.Entry<String, WifiProtocolThread>> connections = mConnectionPool.entrySet();
        for(Map.Entry<String, WifiProtocolThread> connection : connections) {
            macs.add(connection.getValue().getMacAddress());
        }

        return macs;
    }

    public IpAddress getIpByMac(MacAddress macAddress) {
        return mConnectionPool.get(macAddress.toString()).getIpAddress();
    }


}
