package com.mr_thrower.application.service.wifi_module;


import com.mr_thrower.application.database.DbManager;
import library.BytePacket;
import library.ByteUtils;
import library.exceptions.BuildPacketException;
import library.exceptions.EncodingException;
import library.objects.GreetingObject;
import library.wrappers.IpAddress;
import library.wrappers.MacAddress;
import org.apache.log4j.Logger;

import java.io.*;
import java.net.Socket;
import java.net.SocketException;

public class WifiProtocolThread extends Thread {
    private static Logger log = Logger.getLogger(WifiProtocolThread.class.getName());


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private WifiConnectionManager connectionManager;
    private MacAddress macAddress;

    private boolean isRunning;
    private boolean found;

    private Socket socket;

    // streams
    private DataInputStream dataInStream;
    private DataOutputStream dataOutStream;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public WifiProtocolThread(WifiConnectionManager connectionManager, Socket socket, boolean found) {
        isRunning = false;
        macAddress = null;

        this.connectionManager = connectionManager;
        this.socket = socket;
        this.found = found;

        try {
            dataInStream = new DataInputStream(socket.getInputStream());
            dataOutStream = new DataOutputStream(socket.getOutputStream());

        } catch(IOException e) {
            log.info(e.getMessage());
            e.printStackTrace();
        }

        // daemon-thread setting
        setDaemon(true);
        // determining the default priority for the thread
        setPriority(NORM_PRIORITY);
    }


//======================================================================================================================
// GETTERS AND SETTERS
//======================================================================================================================

    public void setMacAddress(MacAddress macAddress) {
        this.macAddress = macAddress;
    }

    public MacAddress getMacAddress() {
        return macAddress;
    }

    public IpAddress getIpAddress() {
        try {
            return new IpAddress(socket.getInetAddress().getAddress());
        } catch (EncodingException e) {
            return null;
        }
    }


//======================================================================================================================
// OPERATING CYCLE
//======================================================================================================================

    @Override
    public void run() {
        isRunning = true;

        if(found) {
            try {
                // send Greeting packet to remote device if it needs
                GreetingObject greeting = DbManager.getInstance().getProfile();
                writeSocket(greeting.getBytePacket());
                log.info("Sent " + greeting.toString());
            } catch(BuildPacketException e) { }
        }

        readSocket();
    }

    @Override
    public void interrupt() {
        isRunning = false;

        try {
            dataInStream.close();
            dataOutStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        super.interrupt();
    }


//======================================================================================================================
// OPERATING METHODS
//======================================================================================================================

    private void readSocket() {
        try {
            // wait and read cycle from the socket
            int codeByte = 0;
            while (isRunning) {
                // waiting for a client message code
                codeByte = dataInStream.readByte();

                // read length of the message
                byte[] lenBytes = new byte[2];
                dataInStream.read(lenBytes);

                try {
                    short length = ByteUtils.toShort(ByteUtils.bytesToObjects(lenBytes));

                    // read message
                    byte[] message = new byte[length - 3];
                    dataInStream.read(message);

                    // wrap a byte array in BytePacket and transfer to ConnectionManager
                    BytePacket packet = new BytePacket((byte) codeByte, lenBytes, message);
                    connectionManager.packetReceived(macAddress, packet);

                } catch (EncodingException e) {
                    log.error(e.getMessage());
                }
            }

        } catch (SocketException e) {
            if(isRunning) {
                e.printStackTrace();
                connectionManager.deleteConnection(this);
            }
        } catch (IOException e) {
            connectionManager.deleteConnection(this);
        }

    }

    public void writeSocket(BytePacket packet) {
        // send message
        try {
            dataOutStream.write(packet.getData());
            dataOutStream.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
