package com.mr_thrower.application.service;

import com.mr_thrower.application.service.bluetooth_module.BtCatchThrow;
import com.mr_thrower.application.service.bluetooth_module.BtThrowThread;
import com.mr_thrower.application.service.wifi_module.WifiCatchThread;
import com.mr_thrower.application.service.wifi_module.WifiThrowThread;
import library.codes.ThrowModeCodes;
import library.interfaces.throw_manager.IThrowManager;
import library.interfaces.throw_manager.IThrowManagerInformer;
import library.interfaces.throw_threads.ICatchThread;
import library.interfaces.throw_threads.IThrowThread;
import library.objects.ThrowEndAckObject;
import library.objects.ThrowFileObject;
import library.wrappers.IpAddress;
import library.wrappers.MacAddress;
import org.apache.log4j.Logger;
import java.util.*;


public class ThrowManager implements IThrowManager, IThrowManagerInformer {
    private static Logger log = Logger.getLogger(ThrowManager.class.getName());


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private EventManager eventManager;
    private Byte mode;

    private final Vector<ThrowFileObject> queueFilesToThrow;
    private final Vector<ThrowFileObject> queueFilesToCatch;

    private final Map<String, IThrowThread> activeThrowThreads;
    private final Map<String, ICatchThread> activeCatchThreads;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    ThrowManager(EventManager eventManager, Byte mode) {
        this.eventManager = eventManager;
        this.mode = mode;

        queueFilesToThrow = new Vector<>();
        queueFilesToCatch = new Vector<>();

        activeThrowThreads = new HashMap<>();
        activeCatchThreads = new HashMap<>();

        log.info("throw manager initialized");
    }

    @Override
    public void stop() {
        // stop active throws and delete from active
        for (Map.Entry<String, IThrowThread> pair : activeThrowThreads.entrySet()) {
            pair.getValue().deactivate();
        }
        for (Map.Entry<String, ICatchThread> pair : activeCatchThreads.entrySet()) {
            pair.getValue().deactivate();
        }

        // clear queues
        synchronized(queueFilesToThrow) {
            activeCatchThreads.clear();
            activeThrowThreads.clear();

            queueFilesToCatch.clear();
            queueFilesToThrow.clear();
        }
    }


//======================================================================================================================
// ADDING THROWS
//======================================================================================================================

    public void addThrowFiles(List<ThrowFileObject> tFileList) {
        synchronized (queueFilesToThrow) {
            queueFilesToThrow.addAll(tFileList);
        }

        log.info(tFileList.size() + " throws was added to throw list");
    }

    public void addCatchFiles(List<ThrowFileObject> tFileList) {
        synchronized (queueFilesToCatch) {
            queueFilesToCatch.addAll(tFileList);
        }

        log.info(tFileList.size() + " throws was added to catch list");
    }


//======================================================================================================================
// START IF NOT YET
//======================================================================================================================

    public void startThrowIfNotYet(MacAddress macAddress, int port) {
        ThrowFileObject tFile = contactCanStartThrow(macAddress);
        if(tFile == null)
            return;

        IThrowThread throwThread;
        switch (mode) {
            case ThrowModeCodes.WIFI_NETWORK:
                throwThread = new WifiThrowThread(this, tFile, macAddress, port);
                break;
            case ThrowModeCodes.BLUETOOTH:
                throwThread = new BtThrowThread(tFile);
                break;
            case ThrowModeCodes.WIFI_DIRECT:
                return;
            default:
                return;
        }

        activeThrowThreads.put(macAddress.toString(), throwThread);
        throwThread.activate();

        log.info("throwing start: " + tFile.getFilename() + " [" + tFile.getTimeID() + "]");
    }

    public void startCatchIfNotYet(MacAddress macAddress) {
        ThrowFileObject tFile = contactCanStartCatch(macAddress);
        if(tFile == null)
            return;

        ICatchThread catchThread;
        switch(mode) {
            case ThrowModeCodes.WIFI_NETWORK:
                catchThread = new WifiCatchThread(this, tFile);
                break;
            case ThrowModeCodes.BLUETOOTH:
                catchThread = new BtCatchThrow(this, tFile);
                break;
            case ThrowModeCodes.WIFI_DIRECT:
                return;
            default:
                return;
        }

        activeCatchThreads.put(macAddress.toString(), catchThread);
        catchThread.activate();

        log.info("catching start: " + tFile.getFilename() + " [" + tFile.getTimeID() + "]");
    }


//======================================================================================================================
// THROW FINISH
//======================================================================================================================

    public void finishThrow(MacAddress macAddress, Date timeID) {
        // stop active throw and delete from active
        IThrowThread throwThread = activeThrowThreads.get(macAddress.toString());
        if(throwThread != null) {
           throwThread.deactivate();
           activeThrowThreads.remove(macAddress.toString());
        }

        // delete from queue
        ThrowFileObject tFileToDelete = null;
        synchronized (queueFilesToThrow) {
            for (ThrowFileObject tFile : queueFilesToThrow) {
                if (macAddress.compareTo(tFile.getAddress()) == 0 && timeID.getTime() == tFile.getTimeID().getTime()) {
                    tFileToDelete = tFile;
                }
            }
            if(tFileToDelete != null) {
                queueFilesToThrow.remove(tFileToDelete);
            }
        }
    }

    public void finishCatch(MacAddress macAddress, Date timeID) {
        // stop active catch and delete from active
        ICatchThread catchThread = activeCatchThreads.get(macAddress.toString());
        if(catchThread != null) {
            catchThread.deactivate();
            activeCatchThreads.remove(macAddress.toString());
        }

        // delete throws from queue
        ThrowFileObject cFileToDelete = null;
        synchronized (queueFilesToCatch) {
            for (ThrowFileObject cFile : queueFilesToCatch) {
                if (macAddress.compareTo(cFile.getAddress()) == 0 && timeID.getTime() == cFile.getTimeID().getTime()) {
                    cFileToDelete = cFile;
                }
            }
            if(cFileToDelete != null) {
                queueFilesToCatch.remove(cFileToDelete);
            }
        }

        // start new catch of contact
        startCatchIfNotYet(macAddress);
    }


//======================================================================================================================
// CONTACT KICKING
//======================================================================================================================

    public void kickUserFromThrows(MacAddress macAddress) {
        // stop active throw and delete from active
        IThrowThread throwThread = activeThrowThreads.get(macAddress.toString());
        if (throwThread != null) {
            throwThread.deactivate();
            activeThrowThreads.remove(macAddress.toString());
        }

        // delete throws from queue
        Vector<ThrowFileObject> throwsToDelete = new Vector<>();
        for (ThrowFileObject tFile : queueFilesToThrow) {
            if (macAddress.compareTo(tFile.getAddress()) == 0) {
                throwsToDelete.add(tFile);
            }
        }
        for (ThrowFileObject tFile : throwsToDelete) {
            synchronized (queueFilesToThrow) {
                queueFilesToThrow.remove(tFile);
            }
        }
    }

    public void kickUserFromCatches(MacAddress macAddress) {
        // stop active throw and delete from active
        ICatchThread catchThread = activeCatchThreads.get(macAddress.toString());
        if (catchThread != null) {
            catchThread.deactivate();
            activeCatchThreads.remove(macAddress.toString());
        }

        // delete throws from queue
        Vector<ThrowFileObject> catchesToDelete = new Vector<>();
        for (ThrowFileObject tFile : queueFilesToCatch) {
            if (macAddress.compareTo(tFile.getAddress()) == 0) {
                catchesToDelete.add(tFile);
            }
        }
        for (ThrowFileObject tFile : catchesToDelete) {
            synchronized (queueFilesToCatch) {
                queueFilesToCatch.remove(tFile);
            }
        }
    }


//======================================================================================================================
// TRANSMITTING
//======================================================================================================================

    @Override
    public void noContact(MacAddress macAddress) {
        kickUserFromThrows(macAddress);
        kickUserFromCatches(macAddress);
    }

    @Override
    public void transmitThrowEndAck(ThrowEndAckObject endAck, String path) {
        eventManager.sendThrowEndAck(endAck, path);

        // end of catching
        finishCatch(endAck.getAddress(), endAck.getTimeID());
    }

    @Override
    public IpAddress getIpAddressByMac(MacAddress macAddress) {
        return eventManager.getIpAddressByMac(macAddress);
    }



//======================================================================================================================
// THROWING STRATEGY
//======================================================================================================================

    private ThrowFileObject contactCanStartThrow(MacAddress macAddress) {
        if(!activeThrowThreads.containsKey(macAddress.toString())) {
            synchronized (queueFilesToThrow) {
                for (ThrowFileObject tFile : queueFilesToThrow) {
                    if (macAddress.compareTo(tFile.getAddress()) == 0) {
                        return tFile;
                    }
                }
            }
        }

        return null;
    }

    private ThrowFileObject contactCanStartCatch(MacAddress macAddress) {
        if(!activeCatchThreads.containsKey(macAddress.toString())) {
            synchronized (queueFilesToCatch) {
                for (ThrowFileObject tFile : queueFilesToCatch) {
                    if (macAddress.compareTo(tFile.getAddress()) == 0) {
                        return tFile;
                    }
                }
            }
        }

        return null;
    }


}
