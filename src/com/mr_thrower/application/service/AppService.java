package com.mr_thrower.application.service;

import com.mr_thrower.application.monitors.NetExpert;
import com.mr_thrower.application.monitors.WifiDataMonitorThread;
import library.objects.GreetingObject;
import org.apache.log4j.Logger;


public class AppService {
    private static Logger log = Logger.getLogger(AppService.class.getName());


//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private GreetingObject profile = new GreetingObject(
            NetExpert.getMacAddress(),
            NetExpert.getIpAddress(),
            "Алексей", // TODO:
            "ASUS",   // TODO:
            GreetingObject.OS_CODE_WINDOWS);


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private static AppService instance;

    private boolean isRunning;
    private Byte mode;

    private EventManager eventManager;


//======================================================================================================================
// SINGLETON CONSTRUCTOR
//======================================================================================================================

    private AppService() {

    }

    public static synchronized AppService getInstance() {
        if (instance == null) {
            instance = new AppService();
        }
        return instance;
    }


//======================================================================================================================
// START AND STOP THE SERVER
//======================================================================================================================

    public void setMode(Byte mode) {
        this.mode = mode;
    }

    public Byte getMode() {
        return mode;
    }

    public void start() {
        if(!isRunning) {
            isRunning = true;

            eventManager = new EventManager(mode);
        }
    }

    public void restart(Byte mode) {
        stop();
        setMode(mode);
        start();
    }

    public void stop() {
        if(isRunning) {
            isRunning = false;
        }

        if(eventManager != null)
            eventManager.stop();
    }


}
