package com.mr_thrower.application.service;

import com.mr_thrower.application.database.DbManager;
import com.mr_thrower.application.monitors.NetExpert;
import com.mr_thrower.application.monitors.WifiDataMonitorThread;
import com.mr_thrower.application.service.bluetooth_module.BtConnectionManager;
import com.mr_thrower.application.service.bluetooth_module.BtDeviceResearchThread;
import com.mr_thrower.application.service.bluetooth_module.IBtDeviceFinder;
import com.mr_thrower.application.service.wifi_module.WifiConnectionManager;
import com.mr_thrower.application.service.wifi_module.WifiDeviceResearchThread;
import library.BytePacket;
import library.codes.*;
import library.exceptions.BuildPacketException;
import library.exceptions.EncodingException;
import library.exceptions.ParsePacketException;
import library.interfaces.IConnectionManager;
import library.interfaces.event_manager.IEventCreator;
import library.interfaces.event_manager.IEventReceiver;
import library.interfaces.event_manager.IEventThrow;
import library.interfaces.throw_manager.IThrowManager;
import library.objects.*;
import library.wrappers.*;
import org.apache.log4j.Logger;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.sqlite.core.DB;

import java.util.ArrayList;
import java.util.Date;


public class EventManager implements IEventReceiver, IEventCreator, IEventThrow {
    private static Logger log = Logger.getLogger(EventManager.class.getName());

//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private final static int WIFI_ADDRESS_RANGE = 255;
    private final static int WIFI_MONITOR_TIMEOUT = 3000;


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private Byte mode;

    private IConnectionManager connectionManager;
    private IThrowManager throwManager;

    // additional threads
    private WifiDataMonitorThread wifiDataMonitorThread;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public EventManager(Byte mode) {
        this.mode = mode;
        wifiDataMonitorThread = null;

        EventBus.getDefault().register(this);

        throwManager = new ThrowManager(this, mode);

        switch(mode) {
            case ThrowModeCodes.WIFI_NETWORK:
                connectionManager = new WifiConnectionManager(this);
                connectionManager.activate();
                startWifiDeviceResearch();
                startWifiDataMonitor();
                break;
            case ThrowModeCodes.BLUETOOTH:
                connectionManager = new BtConnectionManager(this);
                connectionManager.activate();
                startBluetoothDeviceResearch();
                break;
            case ThrowModeCodes.WIFI_DIRECT:
                //startDirectDeviceResearch();
                break;
            default:
                break;
        }

        log.info("event manager initialized");
    }

    public void stop() {
        throwManager.stop();
        if(connectionManager!= null)
            connectionManager.deactivate();

        if(wifiDataMonitorThread != null)
            wifiDataMonitorThread.interrupt();

        EventBus.getDefault().unregister(this);
        log.info("service stopped");
    }


//======================================================================================================================
// DEVICE SEARCH
//======================================================================================================================

    private void startWifiDeviceResearch() {
        String currentIP = NetExpert.getIpAddress().toString();

        // getting subnet in String format
        int firstSeparator = currentIP.lastIndexOf("/");
        int lastSeparator = currentIP.lastIndexOf(".");
        String subnet = currentIP.substring(firstSeparator+1, lastSeparator+1);

        // ping all the addresses of this network
        for (int i = 1; i <= WIFI_ADDRESS_RANGE; i++) {
            String host = subnet + i;

            // exclude checking own IP address
            if(host.equals(currentIP))
                continue;

            new WifiDeviceResearchThread(host, (WifiConnectionManager) connectionManager).start();
        }
    }

    private void startBluetoothDeviceResearch() {
        new BtDeviceResearchThread((IBtDeviceFinder) connectionManager, BtConnectionManager.APP_UUID.toString()).start();
    }

    private void startDirectDeviceResearch() {
        ArrayList<GreetingObject> contacts = DbManager.getInstance().selectAllContacts();
        for(GreetingObject contact : contacts) {
            // fire an event to listeners
            EventBus.getDefault().post(new EventWrapper(contact, EventWrapper.FROM_EVENT_MANAGER));
        }
    }

    private void startWifiDataMonitor() {
        wifiDataMonitorThread = new WifiDataMonitorThread(WIFI_MONITOR_TIMEOUT);
        wifiDataMonitorThread.start();
    }


//======================================================================================================================
// UTILS
//======================================================================================================================

    @Override
    public IpAddress getIpAddressByMac(MacAddress macAddress) {
        return ((WifiConnectionManager) connectionManager).getIpByMac(macAddress);
    }


//======================================================================================================================
// IEventReceiver // EVENT FROM CONNECTIONS
//======================================================================================================================

    // PACKET IDENTIFIER
    @Override
    public void onReceivedPacket(MacAddress sender, BytePacket bytePacket) {
        try {
            switch (bytePacket.getByte(0)) {
                case PacketCodes.GREETING:  // sender is still null !!!
                    onReceivedGreeting(bytePacket);
                    break;
                case PacketCodes.GREETING_ACK:
                    onReceivedGreetingAck(bytePacket);
                    break;
                case PacketCodes.GOODBYE:
                    onReceivedGoodbye(bytePacket);
                    break;
                case PacketCodes.MESSAGE:
                    onReceivedMessage(sender, bytePacket);
                    break;
                case PacketCodes.MESSAGE_ACK:
                    onReceivedMessageAck(sender, bytePacket);
                    break;
                case PacketCodes.THROW_REQUEST:
                    onReceivedThrowRequest(sender, bytePacket);
                    break;
                case PacketCodes.THROW_RESPONSE:
                    onReceivedThrowResponse(sender, bytePacket);
                    break;
                case PacketCodes.THROW_PROGRESS:
                    onReceivedThrowProgress(sender, bytePacket);
                    break;
                case PacketCodes.THROW_END_ACK:
                    onReceivedThrowEndAck(sender, bytePacket);
                    break;

                default:
                    throw new EncodingException("Received [UNKNOWN PACKET CODE]");
            }

        } catch (EncodingException e) {
            // incorrect packet;
            e.printStackTrace();
        }
    }


//======================================================================================================================
// PACKET HANDLERS
//======================================================================================================================

    @Override
    public void onReceivedGreeting(BytePacket packet) {
        try {
            // parse packet to Greeting object
            GreetingObject greeting = new GreetingObject(packet);
            greeting.setAddress(greeting.getMacAddress());
            log.info("Received " + greeting.toString());

            connectionManager.specifyMacToConnection(greeting.getMacAddress());

            // add to database
            DbManager.getInstance().insertContact(greeting);
            // fire an event to listeners
            EventBus.getDefault().post(new EventWrapper(greeting, EventWrapper.FROM_EVENT_MANAGER));

            // sent reciprocal greeting
            GreetingObject greetingAck = DbManager.getInstance().getProfile();
            greetingAck.setAddress(greeting.getMacAddress());
            sendGreetingAck(greetingAck);

        } catch (Exception e) {
            log.error("EXCEPTION: Receiving [GREETING]\n" + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onReceivedGreetingAck(BytePacket packet) {
        try {
            // parse packet to Greeting object
            GreetingObject greetingAck = new GreetingObject(packet);
            greetingAck.setAddress(greetingAck.getMacAddress());
            log.info("Received " + greetingAck.toString());

            connectionManager.specifyMacToConnection(greetingAck.getMacAddress());

            // add to database
            DbManager.getInstance().insertContact(greetingAck);
            // fire an event to listeners
            EventBus.getDefault().post(new EventWrapper(greetingAck, EventWrapper.FROM_EVENT_MANAGER));

        } catch (Exception e) {
            log.error("EXCEPTION: Receiving [GREETING ACK]\n" + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onReceivedGoodbye(BytePacket packet) {
        try {
            // parse packet to Greeting object
            GoodbyeObject goodbye = new GoodbyeObject(packet);
            log.info("Received " + goodbye.toString());

            // fire an event to listeners
            EventBus.getDefault().post(new EventWrapper(goodbye, EventWrapper.FROM_EVENT_MANAGER));

        } catch (Exception e) {
            log.error("EXCEPTION: Receiving [GREETING ACK]\n" + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onReceivedMessage(MacAddress sender, BytePacket packet) {
        try {
            // parse packet to Message object
            MessageObject message = new MessageObject(packet, sender);
            log.info("Received " + message.toString());

            // fire an event to listeners
            EventBus.getDefault().post(new EventWrapper(message, EventWrapper.FROM_EVENT_MANAGER));
            // add to database
            DbManager.getInstance().insertMessage(new MessageWrapper(message, DirectionCodes.INCOMING, mode));

            // send acknowledgment to sender
            MessageAckObject ack = new MessageAckObject(message.getTimeID(), MessageAckObject.CODE_SUCCESS, null);
            ack.setAddress(sender);
            sendMessageAck(ack);

        } catch (ParsePacketException e) {
            log.error("EXCEPTION: Receiving [MESSAGE] from [" + sender.toString() + "]\n" + e.getMessage());
            e.printStackTrace();

            // send acknowledgment to sender
            MessageAckObject ack =
                    new MessageAckObject(new Date(), MessageAckObject.CODE_FAILURE, "ParsePacketException");
            ack.setAddress(sender);
            sendMessageAck(ack);

        }
    }

    @Override
    public void onReceivedMessageAck(MacAddress sender, BytePacket packet) {
        try {
            // parse packet to MessageAck object
            MessageAckObject messageAck = new MessageAckObject(packet, sender);
            log.info("Received " + messageAck.toString());

            // add to database
            DbManager.getInstance().updateMessageState(sender, messageAck.getTimeID(), ThrowStateCodes.STATE_FINISHED);
            // fire an event to listeners
            EventBus.getDefault().post(new EventWrapper(messageAck, EventWrapper.FROM_EVENT_MANAGER));

        } catch (ParsePacketException e) {
            log.error("EXCEPTION: Receiving [MESSAGE ACK] from [" + sender.toString() + "]\n" + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onReceivedThrowRequest(MacAddress sender, BytePacket packet) {
        try {
            // parse packet to ThrowRequest object
            ThrowRequestObject request = new ThrowRequestObject(packet, sender);
            log.info("Received " + request.toString());

            // add to database
            for(ThrowFileObject tFile : request.getThrowFiles()) {
                ThrowWrapper throwWr = new ThrowWrapper(tFile, DirectionCodes.INCOMING, mode, ThrowTypeCodes.FILE);
                DbManager.getInstance().insertThrow(throwWr, ThrowStateCodes.STATE_REQUEST);

            }
            // fire an event to listeners
            EventBus.getDefault().post(new EventWrapper(request, EventWrapper.FROM_EVENT_MANAGER));

        } catch (ParsePacketException e) {
            log.error("EXCEPTION: Receiving [THROW REQUEST] from [" + sender.toString() + "]\n" + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onReceivedThrowResponse(MacAddress sender, BytePacket packet) {
        try {
            // parse packet to ThrowProgress object
            ThrowResponseObject response = new ThrowResponseObject(packet, sender);
            log.info("Received " + response.toString());

            // add to database
            if(response.isNegative()) {
                DbManager.getInstance().updateThrowState(sender, response.getTimeID(), ThrowStateCodes.STATE_REJECTED);
            } else {
                DbManager.getInstance().updateThrowState(sender, response.getTimeID(), ThrowStateCodes.STATE_PROCESS);
            }

            // fire an event to listeners
            EventBus.getDefault().post(new EventWrapper(response, EventWrapper.FROM_EVENT_MANAGER));

            throwManager.startThrowIfNotYet(sender, response.getPort());

        } catch (ParsePacketException e) {
            log.error("EXCEPTION: Receiving [THROW RESPONSE] from [" + sender.toString() + "]\n" + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onReceivedThrowProgress(MacAddress sender, BytePacket packet) {
        try {
            // parse packet to ThrowProgress object
            ThrowProgressObject progress = new ThrowProgressObject(packet, sender);
            log.info("Received " + progress.toString());

            // fire an event to listeners
            EventBus.getDefault().post(new EventWrapper(progress, EventWrapper.FROM_EVENT_MANAGER));

        } catch (ParsePacketException e) {
            log.error("EXCEPTION: Receiving [THROW PROGRESS] from [" + sender.toString() + "]\n" + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onReceivedThrowEndAck(MacAddress sender, BytePacket packet) {
        try {
            // parse packet to ThrowProgress object
            ThrowEndAckObject endAck = new ThrowEndAckObject(packet, sender);
            log.info("Received " + endAck.toString());

            // finish this throw
            throwManager.finishThrow(sender, endAck.getTimeID());

            // add to database
            if(endAck.isSuccess()) {
                DbManager.getInstance().updateThrowState(sender, endAck.getTimeID(), ThrowStateCodes.STATE_FINISHED);
            } else {
                DbManager.getInstance().updateThrowState(sender, endAck.getTimeID(), ThrowStateCodes.STATE_INTERRUPTED);
            }

            // fire an event to listeners
            EventBus.getDefault().post(new EventWrapper(endAck, EventWrapper.FROM_EVENT_MANAGER));

        } catch (ParsePacketException e) {
            log.error("EXCEPTION: Receiving [THROW PROGRESS] from [" + sender.toString() + "]\n" + e.getMessage());
            e.printStackTrace();
        }
    }

    public void onRequestContacts() {
        if(mode == ThrowModeCodes.WIFI_DIRECT)
            startDirectDeviceResearch();
    }


//======================================================================================================================
// IEventCreator // EVENTS FROM GUI
//======================================================================================================================

    @Override
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void sendEvent(EventWrapper event) {
        if(event.getDirection() == EventWrapper.TO_EVENT_MANAGER) {
            switch(event.getCode()) {
                case PacketCodes.GREETING:
                    sendGreetingAck((GreetingObject) event.getObject());
                    break;
                case PacketCodes.MESSAGE:
                    sendMessage((MessageObject) event.getObject());
                    break;
                case PacketCodes.MESSAGE_ACK:
                    sendMessageAck((MessageAckObject) event.getObject());
                    break;
                case PacketCodes.THROW_RESPONSE:
                    sendThrowResponse((ThrowResponseObject) event.getObject());
                    break;
                case PacketCodes.THROW_PROGRESS:
                    sendThrowProgress((ThrowProgressObject) event.getObject());
                    break;
                // case PacketCodes.THROW_END_ACK ->    sendThrowEndAck(event.getObject() as ThrowEndAckObject)
                case PacketCodes.THROW_REQUEST:
                    sendThrowRequest((ThrowRequestObject) event.getObject());
                    break;

                case PacketCodes.REQUEST_CONTACTS:
                    onRequestContacts();
                    break;
            }
        }

        if(event.getDirection() == EventWrapper.TO_THROW_MANAGER) {
            switch (event.getCode()) {
                case PacketCodes.THROW_REQUEST:
                    catchThrowRequest((ThrowRequestObject) event.getObject());
                    break;
            }
        }
    }

//======================================================================================================================
// IEventCreator // SENDERS
//======================================================================================================================

    @Override
    public void sendGreetingAck(GreetingObject greeting) {
        try {
            connectionManager.sendPacket(greeting.getAddress(), greeting.getAckBytePacket());
            log.info("Sent " + greeting.toString());

        } catch (Exception e) {
            log.error("EXCEPTION: Sending [GREETING ACK] to [" + greeting.getAddress().toString() + "]\n" + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void sendMessage(MessageObject message) {
        try {
            connectionManager.sendPacket(message.getAddress(), message.getBytePacket());
            log.info("Sent " + message.toString());

            // write to database
            DbManager.getInstance().insertMessage(new MessageWrapper(message, DirectionCodes.OUTCOMING, mode));

        } catch (Exception e) {
            log.error("EXCEPTION: Sending [MESSAGE] to [" + message.getAddress().toString() + "]\n" + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void sendMessageAck(MessageAckObject messageAck) {
        try {
            connectionManager.sendPacket(messageAck.getAddress(), messageAck.getBytePacket());
            log.info("Sent " + messageAck.toString());

            // write to database
            DbManager.getInstance().updateMessageState(messageAck.getAddress(), messageAck.getTimeID(), ThrowStateCodes.STATE_FINISHED);

        } catch (Exception e) {
            log.error("EXCEPTION: Sending [MESSAGE ACK] to [" + messageAck.getAddress().toString() + "]\n" + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void sendThrowRequest(ThrowRequestObject request) {
        try {
            // add to throw queue
            throwManager.addThrowFiles(request.getThrowFiles());

            // send packet
            connectionManager.sendPacket(request.getAddress(), request.getBytePacket());
            log.info("Sent " + request.toString());

            // write to database
            for(ThrowFileObject tFile : request.getThrowFiles()) {
                ThrowWrapper throwWr = new ThrowWrapper(tFile, DirectionCodes.OUTCOMING, mode, ThrowTypeCodes.FILE);
                DbManager.getInstance().insertThrow(throwWr, ThrowStateCodes.STATE_REQUEST);
            }


        } catch (BuildPacketException e) {
            log.error("EXCEPTION: Sending [THROW REQUEST] to [" + request.getAddress().toString() + "]\n" + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void catchThrowRequest(ThrowRequestObject request) {
        throwManager.addCatchFiles(request.getThrowFiles());
        throwManager.startCatchIfNotYet(request.getAddress());
    }

    @Override
    public void getActualContacts() {
        // NOT USE
    }


//======================================================================================================================
// IEventThrower // EVENTS FROM THROW MANAGER
//======================================================================================================================

    @Override
    public void sendThrowResponse(ThrowResponseObject response) {
        // send packet
        connectionManager.sendPacket(response.getAddress(), response.getBytePacket());
        log.info("Sent " + response.toString());

        // write to database
        if(response.isNegative()) {
            DbManager.getInstance().updateThrowState(response.getAddress(), response.getTimeID(), ThrowStateCodes.STATE_REJECTED);
        } else {
            DbManager.getInstance().updateThrowState(response.getAddress(), response.getTimeID(), ThrowStateCodes.STATE_PROCESS);
        }

        // fire an event to listeners
        EventBus.getDefault().post(new EventWrapper(response, EventWrapper.FROM_EVENT_MANAGER));
    }

    @Override
    public void sendThrowProgress(ThrowProgressObject progress) {
        // send packet
        connectionManager.sendPacket(progress.getAddress(), progress.getBytePacket());
        log.info("Sent " + progress.toString());

        // fire an event to listeners
        EventBus.getDefault().post(new EventWrapper(progress, EventWrapper.FROM_EVENT_MANAGER));
    }

    @Override
    public void sendThrowEndAck(ThrowEndAckObject endAck, String path) {
        // send packet
        connectionManager.sendPacket(endAck.getAddress(), endAck.getBytePacket());
        log.info("Sent " + endAck.toString());

        // write to database
        if(endAck.isSuccess()) {
            DbManager.getInstance().updateThrowState(endAck.getAddress(), endAck.getTimeID(), ThrowStateCodes.STATE_FINISHED);
            DbManager.getInstance().updateThrowPath(endAck.getAddress(), endAck.getTimeID(), path);
        } else {
            DbManager.getInstance().updateThrowState(endAck.getAddress(), endAck.getTimeID(), ThrowStateCodes.STATE_INTERRUPTED);
        }

        // fire an event to listeners
        EventBus.getDefault().post(new EventWrapper(endAck, EventWrapper.FROM_EVENT_MANAGER));
    }


}
