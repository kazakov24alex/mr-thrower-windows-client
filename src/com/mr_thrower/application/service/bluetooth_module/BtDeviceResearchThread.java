package com.mr_thrower.application.service.bluetooth_module;

import com.mr_thrower.application.monitors.BluetoothExpert;
import com.mr_thrower.application.service.bluetooth_module.utils.BtServiceSearch;
import javafx.util.Pair;
import library.wrappers.MacAddress;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;

public class BtDeviceResearchThread extends Thread {
    private static Logger log = Logger.getLogger(BtDeviceResearchThread.class.getName());

//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private IBtDeviceFinder btDeviceFinder;
    private String uuid;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public BtDeviceResearchThread(IBtDeviceFinder btDeviceFinder, String uuid)  {
        this.btDeviceFinder = btDeviceFinder;
        this.uuid = uuid;

        // daemon-thread setting
        setDaemon(true);
        // determining the default priority for the thread
        setPriority(NORM_PRIORITY);
    }


//======================================================================================================================
// OPERATING CYCLE
//======================================================================================================================

    @Override
    public void run()  {
        super.run();

        log.info("Bluetooth device finder search started");

        ArrayList<Pair<MacAddress, StreamConnection>> streamList = new ArrayList<>();

        try {
            String[] searchArgs = new String[] { uuid };
            BtServiceSearch.main(searchArgs);

        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            btDeviceFinder.onDevicesFound(streamList); // empty
            interrupt();
        }

        if (BtServiceSearch.serviceFound.size() == 0) {
            log.info("devices not found");
            btDeviceFinder.onDevicesFound(streamList); // empty
            interrupt();
        }


        for(int i = 0; i < BtServiceSearch.serviceFound.size(); i++) {
            String serverURL = (String) BtServiceSearch.serviceFound.elementAt(i);

            try {
                StreamConnection stream = (StreamConnection) Connector.open(serverURL);
                MacAddress macAddress = BluetoothExpert.extractMacFromBtURL(serverURL);
                streamList.add(new Pair<>(macAddress, stream));

            } catch(Exception e) {
                log.error(e.getMessage());
                e.printStackTrace();
                interrupt();
            }
        }

        log.info("Bluetooth device finder search finished");

        btDeviceFinder.onDevicesFound(streamList);
    }

    @Override
    public void interrupt() {
        super.interrupt();
    }


}