package com.mr_thrower.application.service.bluetooth_module;

import com.mr_thrower.application.service.wifi_module.WifiProtocolThread;
import javafx.util.Pair;
import library.BytePacket;
import library.interfaces.IConnectionManager;
import library.interfaces.event_manager.IEventReceiver;
import library.objects.GoodbyeObject;
import library.wrappers.MacAddress;
import org.apache.log4j.Logger;

import javax.microedition.io.*;
import javax.bluetooth.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


public class BtConnectionManager extends Thread implements IConnectionManager, IBtDeviceFinder {
    private static Logger log = Logger.getLogger(BtConnectionManager.class.getName());

//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private static int POOL_SIZE = 20;
    public static UUID APP_UUID = new UUID("00000000", true);
    private static String APP_SERVICE_URL = "btspp://localhost:" + APP_UUID +";name=MrThrower";


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private IEventReceiver eventManager;
    private Map<String, BtProtocolThread> mConnectionPool;     // String = MacAddress.toString()

    private boolean isRunning;

    private StreamConnectionNotifier streamConnNotifier;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public BtConnectionManager(IEventReceiver eventManager) {
        this.eventManager = eventManager;

        isRunning = false;
        mConnectionPool = new HashMap<String, BtProtocolThread>() {};
    }


//======================================================================================================================
// IConnectionManager
//======================================================================================================================
    @Override
    public void activate() {
        start();
    }

    @Override
    public void deactivate() {
        interrupt();
    }

    @Override
    public void specifyMacToConnection(MacAddress macAddress) {
        BtProtocolThread connection = mConnectionPool.get(null);

        if(connection != null) {
            connection.setMacAddress(macAddress);

            mConnectionPool.put(macAddress.toString(), connection);
            mConnectionPool.remove(null);
        }

        log.info("mac address " + macAddress.toString() + " was specified for connection");
    }

    @Override
    public void sendPacket(MacAddress macAddress, BytePacket packet) {
       mConnectionPool.get(macAddress.toString()).writeSocket(packet);
    }


//======================================================================================================================
// Thread // OPERATING CYCLE
//======================================================================================================================
    @Override
    public void run() {
        super.run();
        isRunning = true;
        log.info("BT connection manager started");

        try {
            while (isRunning) {
                // creating server socket on APP_UUID
                streamConnNotifier = (StreamConnectionNotifier) Connector.open(APP_SERVICE_URL);
                log.info("ServerSocket opened on UUID = " + APP_SERVICE_URL);
                log.info("Waiting for clients to connect...");

                // waiting client connection
                StreamConnection connection = streamConnNotifier.acceptAndOpen();

                // connection occurred
                log.info("... client accepted");

                // create a thread to process the connection
                BtProtocolThread btThread = new BtProtocolThread(this, connection, false);
                addConnection(new MacAddress(), btThread);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void interrupt() {
        // stop server socket
        try {
            if (streamConnNotifier != null)
                streamConnNotifier.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        // stop connection threads
        for (Map.Entry<String, BtProtocolThread> pair : mConnectionPool.entrySet()) {
            pair.getValue().interrupt();
        }
        mConnectionPool.clear();

        isRunning = false;

        log.info("BT connection manager stopped");
        super.interrupt();
    }


//======================================================================================================================
// OPERATING METHODS
//======================================================================================================================

    public void packetReceived(MacAddress macAddress, BytePacket bytePacket) {
        eventManager.onReceivedPacket(macAddress, bytePacket);
    }

    @Override
    public void onDevicesFound(ArrayList<Pair<MacAddress, StreamConnection>> streams) {
        for(Pair<MacAddress, StreamConnection> pair : streams) {
            BtProtocolThread btThread = new BtProtocolThread(this, pair.getValue(), true);
            addConnection(new MacAddress(), btThread);
        }
    }

    private void addConnection(MacAddress macAddress, BtProtocolThread connection) {
        if(mConnectionPool.size() < POOL_SIZE) {
            mConnectionPool.put(macAddress.toString(), connection);

            log.info("connection was added to Connection Pool");
            connection.start();

        } else {
            log.info("connection was rejected (Connection Pool is overflowed");
            connection.interrupt();
        }
    }

    public void deleteConnection(BtProtocolThread connectionThread) {
        mConnectionPool.remove(connectionThread);

        // emulate Goodbye packet receiving
        GoodbyeObject goodbye = new GoodbyeObject(connectionThread.getMacAddress());
        eventManager.onReceivedPacket(connectionThread.getMacAddress(), goodbye.getBytePacket());

        log.info("device ["+connectionThread.getMacAddress().toString()+"] was disconnected");
    }


//======================================================================================================================
// AUXILIARY METHODS
//======================================================================================================================

    @Override
    public ArrayList<MacAddress> getActualMacAddresses() {
        ArrayList<MacAddress> macs = new ArrayList<>();

        Set<Map.Entry<String, BtProtocolThread>> connections = mConnectionPool.entrySet();
        for(Map.Entry<String, BtProtocolThread> connection : connections) {
            macs.add(connection.getValue().getMacAddress());
        }

        return macs;
    }


}

