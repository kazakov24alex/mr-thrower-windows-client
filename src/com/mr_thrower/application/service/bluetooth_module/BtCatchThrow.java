package com.mr_thrower.application.service.bluetooth_module;

import library.interfaces.throw_manager.IThrowManagerInformer;
import library.interfaces.throw_threads.ICatchThread;
import library.objects.ThrowEndAckObject;
import library.objects.ThrowFileObject;
import library.objects.ThrowProgressObject;
import library.objects.ThrowResponseObject;
import library.wrappers.EventWrapper;
import org.apache.log4j.Logger;
import org.greenrobot.eventbus.EventBus;

import javax.bluetooth.UUID;
import javax.microedition.io.Connector;
import javax.microedition.io.StreamConnection;
import javax.microedition.io.StreamConnectionNotifier;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


public class BtCatchThrow extends Thread implements ICatchThread {
    private static Logger log = Logger.getLogger(BtCatchThrow.class.getName());

//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private final int CHUNK_SIZE = 32768;
    private final long PROGRESS_UPDATE_LIMIT = 100 * 1024;  // bytes


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private IThrowManagerInformer throwManager;
    private ThrowFileObject catchFile;
    private String catchURL;

    private boolean isCompleted;

    private StreamConnectionNotifier streamConnNotifier;
    private StreamConnection connection;

    private DataInputStream dataInputStream;
    private FileOutputStream fileOutputStream;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public BtCatchThrow(IThrowManagerInformer throwManager, ThrowFileObject catchFile) {
        this.throwManager = throwManager;
        this.catchFile = catchFile;

        UUID catchUUID = new UUID(catchFile.getUUIDstart(), true);
        catchURL = "btspp://localhost:" + catchUUID +";name=MrThrower";

        isCompleted = false;

        // daemon-thread setting
        setDaemon(true);
        // determining the default priority for the thread
        setPriority(NORM_PRIORITY);
    }


//======================================================================================================================
// ICatchThread
//======================================================================================================================

    @Override
    public void activate() {
        super.start();
    }

    @Override
    public void deactivate() {
        super.interrupt();
    }


//======================================================================================================================
// OPERATING CYCLE
//======================================================================================================================

    @Override
    public void run() {
        log.info("thread started");

        connectToThrower();
        catchFile();
    }


    @Override
    public void interrupt() {
        try {
            if(fileOutputStream != null)
                fileOutputStream.close();
            if(dataInputStream != null)
                dataInputStream.close();
            if(connection != null)
                connection.close();
            if(streamConnNotifier != null)
                streamConnNotifier.close();

            if(!isCompleted) {
                // TODO delete file
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        log.info("thread stopped");
        super.interrupt();
    }



    private void connectToThrower() {
        try {
            // create server socket on certain URL
            streamConnNotifier = (StreamConnectionNotifier) Connector.open(catchURL);
            log.info("Waiting for thrower to connect...");

            // send ThrowResponse with file TimeID
            ThrowResponseObject response = new ThrowResponseObject(catchFile.getTimeID(), 0);
            response.setAddress(catchFile.getAddress());
            EventBus.getDefault().post(new EventWrapper(response, EventWrapper.TO_EVENT_MANAGER));

            // waiting for thrower
            connection = streamConnNotifier.acceptAndOpen();
            log.info("... thrower connected");

        } catch(IOException e) {
            e.printStackTrace();
        }
    }

    private void catchFile() {
        log.info("BT-Catch process started");



        try {
            dataInputStream = connection.openDataInputStream();

            // limit of progress dispatch
            long progressLimit = PROGRESS_UPDATE_LIMIT;

            long bytesReceived = 0;

            fileOutputStream = new FileOutputStream(this.catchFile.getFilename());

            while (true){
                // read in pieces
                byte[] chunk = new byte[CHUNK_SIZE];
                int readBytesCount = dataInputStream.read(chunk);

                if (readBytesCount > 0) {
                    fileOutputStream.write(chunk, 0, readBytesCount);

                    bytesReceived += readBytesCount;

                    // send progress
                    if(bytesReceived > progressLimit) {
                        ThrowProgressObject progress = new ThrowProgressObject(this.catchFile.getTimeID(), bytesReceived);
                        progress.setAddress(catchFile.getAddress());
                        EventBus.getDefault().post(new EventWrapper(progress, EventWrapper.TO_EVENT_MANAGER));

                        progressLimit += PROGRESS_UPDATE_LIMIT;
                    }

                    if (bytesReceived == catchFile.getFileSize())
                        break;
                }
            }

            // close file
            fileOutputStream.flush();
            fileOutputStream.close();
            fileOutputStream = null;

            isCompleted = (bytesReceived == catchFile.getFileSize());

            // send end ack notification
            final String dir = System.getProperty("user.dir");
            ThrowEndAckObject endAck = new ThrowEndAckObject(this.catchFile.getTimeID(), isCompleted);
            endAck.setAddress(catchFile.getAddress());
            throwManager.transmitThrowEndAck(endAck, dir + "\\" + catchFile.getFilename()); // TODO path

        } catch (IOException e) {
            log.error(e.getMessage());
            e.printStackTrace();
        }

        log.info("BT-Catch process finished");
    }

}
