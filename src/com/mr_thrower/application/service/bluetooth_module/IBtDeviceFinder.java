package com.mr_thrower.application.service.bluetooth_module;

import javafx.util.Pair;
import library.wrappers.MacAddress;

import javax.microedition.io.StreamConnection;
import java.util.ArrayList;

public interface IBtDeviceFinder {

    void onDevicesFound(ArrayList<Pair<MacAddress, StreamConnection>> streams);

}
