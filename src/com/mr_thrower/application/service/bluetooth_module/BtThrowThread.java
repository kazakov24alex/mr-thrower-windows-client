package com.mr_thrower.application.service.bluetooth_module;

import javafx.util.Pair;
import library.interfaces.throw_threads.IThrowThread;
import library.objects.ThrowFileObject;
import library.wrappers.MacAddress;
import org.apache.log4j.Logger;

import javax.bluetooth.UUID;
import javax.microedition.io.StreamConnection;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;


public class BtThrowThread extends Thread implements IThrowThread, IBtDeviceFinder {
    private static Logger log = Logger.getLogger(BtThrowThread.class.getName());

//======================================================================================================================
// CONSTANTS
//======================================================================================================================

    private final static int CHUNK_SIZE = 32768;
    private final static int NUM_CONNECT_ATTEMPS = 5;


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private ThrowFileObject throwFile;

    private boolean isRunning;
    private int numConnectionAttemps;

    private StreamConnection streamConnection;
    private OutputStream outputStream;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public BtThrowThread(ThrowFileObject throwFile) {
        this.throwFile = throwFile;

        isRunning = false;
        numConnectionAttemps = 0;

        // daemon-thread setting
        setDaemon(true);
        // determining the default priority for the thread
        setPriority(NORM_PRIORITY);
    }


//======================================================================================================================
// ICatchThread
//======================================================================================================================

    @Override
    public void activate() {
        log.info("Searching catching device...");
        UUID throw_uuid = new UUID(throwFile.getUUIDstart(), true);
        log.info("on UUID = " + throw_uuid.toString());
        new BtDeviceResearchThread(this, throw_uuid.toString()).start();
    }

    @Override
    public void deactivate() {
        super.interrupt();
    }

    @Override
    public Date getTimeID() {
        return throwFile.getTimeID();
    }


//======================================================================================================================
// IBtDeviceFinder
//======================================================================================================================

    @Override
    public void onDevicesFound(ArrayList<Pair<MacAddress, StreamConnection>> streams) {
        if(streams.isEmpty()) {
            /*log.info("... catching device was not found");
            interrupt();*/
            log.info("EEEEEEEMPTY");
        }

        if(streams.size() !=0) {
            streamConnection = streams.get(0).getValue();
            log.info("... catching device was found");

            start();

        } /*else {
            if(numConnectionAttemps < NUM_CONNECT_ATTEMPS) {
                numConnectionAttemps++;
                log.info("connection attempt " + numConnectionAttemps);
                activate();
            }
        }*/
    }


//======================================================================================================================
// OPERATING CYCLE
//======================================================================================================================

    @Override
    public void run() {
        log.info("thread started");
        isRunning = true;

        throwFile();
    }

    @Override
    public void interrupt() {
        isRunning = false;

        try {
            if (outputStream != null)
                outputStream.close();

            if (streamConnection != null)
                streamConnection.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        log.info("thread finished");
        super.interrupt();
    }


//======================================================================================================================
// OPERATING METHODS
//======================================================================================================================

    private void throwFile() {
        log.info("BT-throw process started");

        try {
            outputStream = streamConnection.openOutputStream();

            FileInputStream fis = new FileInputStream(throwFile.getPath());

            // send file in pieces (CHUNKS)
            long bytesLeft = throwFile.getFileSize();
            byte[] chunkBuffer = new byte[CHUNK_SIZE];

            while(bytesLeft > CHUNK_SIZE) {
                fis.read(chunkBuffer);
                outputStream.write(chunkBuffer, 0, CHUNK_SIZE);

                bytesLeft -= CHUNK_SIZE;
            }

            // send file reminder
            byte[] remainderBuffer = new byte[(int) bytesLeft];
            fis.read(remainderBuffer);
            outputStream.write(remainderBuffer, 0, remainderBuffer.length);
            outputStream.flush();

            // close file stream
            fis.close();

        } catch(IOException e) {
            e.printStackTrace();
        }

        log.info("BT-throw process finished");
    }


}