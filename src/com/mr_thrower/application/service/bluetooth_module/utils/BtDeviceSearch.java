package com.mr_thrower.application.service.bluetooth_module.utils;


import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Vector;
import javax.bluetooth.*;

/**
 * Minimal Device Discovery example.
 */
public class BtDeviceSearch {
    private static Logger log = Logger.getLogger(BtDeviceSearch.class.getName());

    public static final Vector/*<RemoteDevice>*/ devicesDiscovered = new Vector();

    public static void main(String[] args) throws IOException, InterruptedException {

        final Object inquiryCompletedEvent = new Object();

        devicesDiscovered.clear();

        DiscoveryListener listener = new DiscoveryListener() {

            public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {
                devicesDiscovered.addElement(btDevice);
                try {
                    log.info("Device found: " + btDevice.getBluetoothAddress() + " [" + btDevice.getFriendlyName(false) + "]");
                } catch (IOException cantGetDeviceName) {
                }
            }

            public void inquiryCompleted(int discType) {
                // device Inquiry completed!
                synchronized(inquiryCompletedEvent){
                    inquiryCompletedEvent.notifyAll();
                }
            }

            public void serviceSearchCompleted(int transID, int respCode) {
            }

            public void servicesDiscovered(int transID, ServiceRecord[] servRecord) {
            }
        };

        synchronized(inquiryCompletedEvent) {
            boolean started = LocalDevice.getLocalDevice().getDiscoveryAgent().startInquiry(DiscoveryAgent.GIAC, listener);
            if (started) {
                // wait for device inquiry to complete...
                inquiryCompletedEvent.wait();
                log.info(devicesDiscovered.size() +  " device(s) found");
            }
        }
    }

}