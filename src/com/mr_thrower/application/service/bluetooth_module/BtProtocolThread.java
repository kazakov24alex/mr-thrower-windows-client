package com.mr_thrower.application.service.bluetooth_module;

import com.mr_thrower.application.database.DbManager;
import library.BytePacket;
import library.ByteUtils;
import library.exceptions.BuildPacketException;
import library.exceptions.EncodingException;
import library.objects.GreetingObject;
import library.wrappers.MacAddress;
import org.apache.log4j.Logger;

import javax.microedition.io.StreamConnection;
import java.io.*;
import java.net.SocketException;


public class BtProtocolThread extends Thread {
    private static Logger log = Logger.getLogger(BtProtocolThread.class.getName());


//======================================================================================================================
// MEMBERS
//======================================================================================================================

    private BtConnectionManager connectionManager;
    private MacAddress macAddress;

    private StreamConnection streamConnection;

    private boolean found;
    private boolean isRunning;

    // streams
    private DataInputStream dataInStream;
    private DataOutputStream dataOutStream;


//======================================================================================================================
// CONSTRUCTOR
//======================================================================================================================

    public BtProtocolThread(BtConnectionManager connectionManager, StreamConnection streamConnection, boolean found) {
        this.connectionManager = connectionManager;
        this.macAddress = null;
        this.streamConnection = streamConnection;

        isRunning = false;
        this.found = found;

        try {
            dataInStream = streamConnection.openDataInputStream();
            dataOutStream = streamConnection.openDataOutputStream();

        } catch(IOException e) {
            log.info(e.getMessage());
            e.printStackTrace();
        }

        // daemon-thread setting
        setDaemon(true);
        // determining the default priority for the thread
        setPriority(NORM_PRIORITY);
    }


//======================================================================================================================
// GETTERS
//======================================================================================================================

    public MacAddress getMacAddress() {
        return  macAddress;
    }

    public void setMacAddress(MacAddress macAddress) {
        this.macAddress = macAddress;
    }


//======================================================================================================================
// OPERATING CYCLE
//======================================================================================================================

    @Override
    public void run() {
        isRunning = true;

        // send Greeting packet to remote device if it needs
        if(!found) {
            try {
                GreetingObject greeting = DbManager.getInstance().getProfile();
                writeSocket(greeting.getBytePacket());
                log.info("Sent " + greeting.toString());
            } catch(BuildPacketException e) { }
        }

        readSocket();
    }

    @Override
    public void interrupt() {
        isRunning = false;

        try {
            dataInStream.close();
            dataOutStream.close();
            streamConnection.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        super.interrupt();
    }


//======================================================================================================================
// READ AND WRITE SOCKET
//======================================================================================================================

    private void readSocket() {
        try {
            // wait and read cycle from the socket
            int codeByte = 0;
            while (isRunning) {
                // waiting for a client message code
                codeByte = dataInStream.readByte();

                // read length of the message
                byte[] lenBytes = new byte[2];
                dataInStream.read(lenBytes);

                try {
                    short length = ByteUtils.toShort(ByteUtils.bytesToObjects(lenBytes));

                    // read message
                    byte[] message = new byte[length - 3];
                    dataInStream.read(message);

                    // wrap a byte array in BytePacket and transfer to BtConnectionManager
                    BytePacket packet = new BytePacket((byte) codeByte, lenBytes, message);
                    connectionManager.packetReceived(macAddress, packet);

                } catch (EncodingException e) {
                    e.printStackTrace();
                }
            }

        } catch (SocketException e) {
            if(isRunning) {
                e.printStackTrace();
                connectionManager.deleteConnection(this);
            }
        } catch (IOException e) {
            connectionManager.deleteConnection(this);
        }

    }

    public void writeSocket(BytePacket packet) {
        try {
            // send message
            dataOutStream.write(packet.getData());
            dataOutStream.flush();

        } catch (Exception e) {
            log.error("error sending protocol packet");
            e.printStackTrace();
        }
    }


}
